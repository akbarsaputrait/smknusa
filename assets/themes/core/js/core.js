$(document).ready(function () {
    $('body').bootstrapMaterialDesign();

    $('.lazy').lazy({
        effect: "show",
        effectTime: 100,
        placeholder: config.baseURL + "assets/themes/default/img/loader.gif"
    });
});