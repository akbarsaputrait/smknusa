<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Admin Template
 */
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?= $this->settings->site_version; ?>">
    <link rel="icon" type="image/x-icon" href="/favicon.ico?v=<?= $this->settings->site_version; ?>">
    <title><?= $page_title; ?> - <?= $this->settings->site_name; ?></title>

    <?php // CSS files ?>
    <?php if (isset($css_files) && is_array($css_files)) : ?>
        <?php foreach ($css_files as $css) : ?>
            <?php if (!is_null($css)) : ?>
                <?php $separator = (strstr($css, '?')) ? '&' : '?'; ?>
                <link rel="stylesheet"
                      href="<?= $css; ?><?= $separator; ?>v=<?= $this->settings->site_version; ?>"><?= "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        var base_url = '<?= base_url();?>';
    </script>
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
    <nav class="navbar page-header">
        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none mr-auto">
            <i class="fa fa-bars"></i>
        </a>
        <a class="navbar-brand d-flex justify-content-center align-items-center" href="<?= base_url();?>">
            <img src="<?= base_url("/{$this->settings->themes_folder}/admin/img/logo_smk_black.png") ?>" width="120"
                 alt="logo">
        </a>
        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
            <i class="fa fa-bars"></i>
        </a>

        <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <img src="<?= base_url("/uploads/img/photo_profile/" . $this->user['photo']) ?>"
                         class="avatar avatar-sm" alt="logo">
                    <span class="small ml-1 d-md-down-none">
                            <?= $this->user['name'] ?>
                        </span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header">Account</div>

                    <a href="<?= base_url('admin/profile') ?>" class="dropdown-item">
                        <i class="fa fa-user"></i> Profile
                    </a>

                    <a href="<?= base_url('logout'); ?>" class="dropdown-item">
                        <i class="fa fa-lock"></i> Logout
                    </a>
                </div>
            </li>
        </ul>
    </nav>
    <div class="main-container">
        <!-- START SIDEBAR -->
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">

                    <li class="nav-title">Home</li>

                    <li class="nav-item">
                        <a href="<?= base_url('admin/dashboard') ?>"
                           class="nav-link <?php echo (uri_string() == 'admin/dashboard') ? 'active' : ''; ?>">
                            <i class="icon icon-speedometer"></i> Dashboard
                        </a>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-notebook"></i> Blog
                            <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?= base_url('admin/blog') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/blog') ? 'active' : ''; ?>">
                                    <i class="icon icon-list"></i> Daftar Blog
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('admin/blog/add') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/blog/add') ? 'active' : ''; ?>">
                                    <i class="icon icon-plus"></i> Tambah Blog
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="fa fa-exclamation"></i> Pengumuman
                            <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?= base_url('admin/pengumuman') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/pengumuman') ? 'active' : ''; ?>">
                                    <i class="icon icon-list"></i> Daftar Pengumuman
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('admin/pengumuman/add') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/pengumuman/add') ? 'active' : ''; ?>">
                                    <i class="icon icon-plus"></i> Tambah Pengumuman

                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-picture"></i> Galeri
                            <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?= base_url('admin/galeri'); ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/galeri') ? 'active' : ''; ?>">
                                    <i class="icon icon-list"></i> Daftar Foto
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('admin/galeri/add'); ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/galeri/add') ? 'active' : ''; ?>">
                                    <i class="icon icon-plus"></i> Tambah Foto
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('admin/agenda') ?>"
                           class="nav-link <?php echo (uri_string() == 'admin/agenda') ? 'active' : ''; ?>">
                            <i class="icon icon-calendar"></i> Agenda
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= base_url('admin/link') ?>"
                           class="nav-link <?php echo (uri_string() == 'admin/link') ? 'active' : ''; ?>">
                            <i class="icon icon-link"></i> Link Eksternal
                        </a>
                    </li>

                    <li class="nav-title">Sekolah</li>

                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-people"></i> Guru
                            <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?= base_url('admin/guru') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/guru') ? 'active' : ''; ?>">
                                    <i class="icon icon-link"></i> Daftar Guru
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('admin/guru/add') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/guru/add') ? 'active' : ''; ?>">
                                    <i class="icon icon-plus"></i> Tambah Guru
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-star"></i> Fasilitas
                            <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?= base_url('admin/fasilitas') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/fasilitas') ? 'active' : ''; ?>">
                                    <i class="icon icon-list"></i> Daftar Fasilitas
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= base_url('admin/fasilitas/add') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/fasilitas/add') ? 'active' : ''; ?>">
                                    <i class="icon icon-plus"></i> Tambah Fasilitas
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="#" class="nav-link nav-dropdown-toggle">
                            <i class="icon icon-trophy"></i> Program Keahlian
                            <i class="fa fa-caret-left"></i>
                        </a>

                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="<?= base_url('admin/program_keahlian/list/informatika') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/program_keahlian/list/informatika') ? 'active' : ''; ?>">
                                    <i class="fa fa-desktop"></i> Teknik Informatika
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('admin/program_keahlian/list/pertanian') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/program_keahlian/list/pertanian') ? 'active' : ''; ?>">
                                    <i class="fa fa-leaf"></i> Teknik Pertanian
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('admin/program_keahlian/list/mesin') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/program_keahlian/list/mesin') ? 'active' : ''; ?>">
                                    <i class="fa fa-cogs"></i> Teknik Mesin
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= base_url('admin/program_keahlian/list/elektro') ?>"
                                   class="nav-link <?php echo (uri_string() == 'admin/program_keahlian/list/elektro') ? 'active' : ''; ?>">
                                    <i class="fa fa-bolt"></i> Teknik Elektro
                                </a>
                            </li>
                        </ul>

                    <li class="nav-item">
                        <a href="<?= base_url('admin/alumni') ?>"
                           class="nav-link <?php echo (uri_string() == 'admin/alumni') ? 'active' : ''; ?>">
                            <i class="icon icon-people"></i> Alumni
                        </a>
                    </li>
                    </li>

                    <li class="nav-title">Website</li>
                    <li class="nav-item">
                        <a href="<?= base_url('admin/settings') ?>"
                           class="nav-link <?php echo (uri_string() == 'admin/settings') ? 'active' : ''; ?>">
                            <i class="icon icon-wrench"></i> Pengaturan
                        </a>
                    </li>

                    <!--                    </li>-->
                </ul>
            </nav>
        </div>
        <!-- END SIDEBAR -->

        <!-- START CONTENT -->
        <div class="content">
            <?php // System messages ?>
            <?php if ($this->session->flashdata('message')) : ?>
                <div class="alert alert-success alert-dismissable notif">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $this->session->flashdata('message'); ?>
                </div>
            <?php elseif ($this->session->flashdata('error')) : ?>
                <div class="alert alert-danger alert-dismissable notif">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $this->session->flashdata('error'); ?>
                </div>
            <?php elseif (validation_errors()) : ?>
                <div class="alert alert-danger alert-dismissable notif">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= validation_errors(); ?>
                </div>
            <?php elseif ($this->error) : ?>
                <div class="alert alert-danger alert-dismissable notif">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?= $this->error; ?>
                </div>
            <?php endif; ?>
            <?php // Start Content ?>
            <?= $content; ?>
        </div>
    </div>
    <footer class="p-2 w-100 bg-light">
        <div class="d-flex justify-content-center align-items-center">
            <span><b>&copy; Copyright</b> Akbar Anung Yudha Saputra, 2018</span>
        </div>
    </footer>
</div>

<?php // Javascript files ?>
<?php if (isset($js_files) && is_array($js_files)) : ?>
    <?php foreach ($js_files as $js) : ?>
        <?php if (!is_null($js)) : ?>
            <?php $separator = (strstr($js, '?')) ? '&' : '?'; ?>
            <?= "\n"; ?>
            <script type="text/javascript"
                    src="<?= $js; ?><?= $separator; ?>v=<?= $this->settings->site_version; ?>"></script><?= "\n"; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
<?php if (isset($js_files_i18n) && is_array($js_files_i18n)) : ?>
    <?php foreach ($js_files_i18n as $js) : ?>
        <?php if (!is_null($js)) : ?>
            <?= "\n"; ?>
            <script type="text/javascript"><?= "\n" . $js . "\n"; ?></script><?= "\n"; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
</body>
</html>
