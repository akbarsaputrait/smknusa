$(document).ready(function () {
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    var myDropzone = new Dropzone(document.getElementById("actions"), {
        url: base_url + "admin/guru/add_photo", // mengatur url
        thumbnailWidth: 100,
        thumbnailHeight: 100,
        parallelUploads: 20,
        maxFilesize: 10, // membatasi ukuran file yang di upload
        previewTemplate: previewTemplate,
        acceptedFiles: "image/*",
        autoQueue: false, // Pastikan bahwa file tidak antri sampai ditambahkan secara manual
        previewsContainer: "#previews", // menentukan elemen untuk menampilkan preview
        clickable: ".fileinput-button" // menentukan elemen pemicu untuk memilih file
    });

    myDropzone.on("addedfile", function (file) {
        if (this.files.length) {
            var _i, _len;
            for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
            {
                if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                    this.removeFile(file);
                }
            }
        }

        var unique_field_id = new Date().getTime();

        // INPUT NAMA
        nama = file.nama == undefined ? "" : file.nama;
        file._namaBox = Dropzone.createElement("<div class='col-md-3'><div class='form-group'><input id='" + file.name + unique_field_id + "_namaBox' required class='form-control' type='text' placeholder='Nama Guru' name='teacher_name' value=" + nama + " ></div></div>");
        file.previewElement.appendChild(file._namaBox);

        // INPUT PROFESI
        profession = file.profession == undefined ? "" : file.profession;
        file._professionBox = Dropzone.createElement("<div class='col-md-3'><div class='form-group'><input id='" + file.name + unique_field_id + "_professionBox' required class='form-control' type='text' placeholder='Profesi Guru' name='teacher_profession' value=" + profession + " ></div></div>");
        file.previewElement.appendChild(file._professionBox);

        // INPUT TAHUN
        year = file.year == undefined ? "" : file.year;
        file._yearBox = Dropzone.createElement("<div class='col-md-3'><div class='form-group'><input id='" + file.name + unique_field_id + "_yearBox' required class='form-control' type='text' placeholder='Sejak Tahun' name='teacher_year' value=" + year + " ></div></div>");
        file.previewElement.appendChild(file._yearBox);

        // INPUT EMAIL
        email = file.email == undefined ? "" : file.email;
        file._emailBox = Dropzone.createElement("<div class='col-md-3'><div class='form-group'><input id='" + file.name + unique_field_id + "_emailBox' required class='form-control' type='email' placeholder='Email' name='teacher_email' value=" + email + " ></div></div>");
        file.previewElement.appendChild(file._emailBox);

        // menghubungkan tombol trart
        file.previewElement.querySelector(".start").onclick = function () {
            myDropzone.enqueueFile(file);
        };
    });

    myDropzone.on("sending", function (file, xhr, formData) {
        nama = file.previewElement.querySelector("input[name='teacher_name']");
        profesi = file.previewElement.querySelector("input[name='teacher_profession']");
        year = file.previewElement.querySelector("input[name='teacher_year']");
        email = file.previewElement.querySelector("input[name='teacher_email']");

        // format of this depends on your paramName config. Mine was called image
        formData.append("teacher_name", $(nama).val());
        formData.append("teacher_profession", $(profesi).val());
        formData.append("teacher_year", $(year).val());
        formData.append("teacher_email", $(email).val());

        // menampilkan total progressbar
        document.querySelector("#total-progress").style.opacity = "1";
        // pada saat upload berlangsung, tombol start akan mati(disabled)
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
    });

    myDropzone.on("success", function (response) {
        if (response.status == "success") {
            swal({
                type: 'success',
                title: 'Berhasil!',
                showConfirmButton: false,
                timer: 1500
            });
        } else {
            swal({
                type: 'error',
                title: 'gagal!',
                showConfirmButton: false,
                timer: 1500
            });
        }
    });

// progressbar akan di sembunyikan ketika prosess upload sudah selesai
    myDropzone.on("queuecomplete", function (progress) {
        document.querySelector("#total-progress").style.opacity = "0";
    });

    myDropzone.on("complete", function (file, progress) {
        document.querySelector("#total-progress").style.opacity = "0";
        this.removeFile(file);
    });

// Membuat fungsi mengunggah semua gambar pada tombol start
    document.querySelector("#actions .start").onclick = function () {
        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
    };
// Membuat fungsi pembatalan semua gambar pada saat upload
    document.querySelector("#actions .cancel").onclick = function (file) {
        myDropzone.fileupload - process(true);
    };

    document.querySelector("button.removeAll").onclick = function () {
        myDropzone.removeAllFiles();
    };
})