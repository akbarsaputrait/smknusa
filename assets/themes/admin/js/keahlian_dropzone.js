$(document).ready(function () {
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    var myDropzone = new Dropzone(document.getElementById("actions"), {
        url: config.baseURL + "admin/program_keahlian/add_photo/" + class_id, // mengatur url
        thumbnailWidth: 100,
        thumbnailHeight: 100,
        parallelUploads: 20,
        maxFilesize: 10, // membatasi ukuran file yang di upload
        previewTemplate: previewTemplate,
        acceptedFiles: "image/*",
        autoQueue: false, // Pastikan bahwa file tidak antri sampai ditambahkan secara manual
        previewsContainer: "#previews", // menentukan elemen untuk menampilkan preview
        clickable: ".fileinput-button" // menentukan elemen pemicu untuk memilih file
    });

    myDropzone.on("addedfile", function (file) {
        if (this.files.length) {
            var _i, _len;
            for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
            {
                if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                    this.removeFile(file);
                }
            }
        }

        // menghubungkan tombol trart
        file.previewElement.querySelector(".start").onclick = function () {
            myDropzone.enqueueFile(file);
        };

    });

    myDropzone.on("sending", function (file, xhr, formData) {
        // menampilkan total progressbar
        document.querySelector("#total-progress").style.opacity = "1";
        // pada saat upload berlangsung, tombol start akan mati(disabled)
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
    });

    myDropzone.on("success", function (response) {
        if (response.status == "success") {
            swal({
                type: 'success',
                title: 'Berhasil!',
                showConfirmButton: true,
            }).then((result) => {
                if (result.value) {
                    location.reload(true);
                }
            });
        } else {
            swal({
                type: 'error',
                title: 'gagal!',
                showConfirmButton: false,
                timer: 1500
            });
        }
    });

// progressbar akan di sembunyikan ketika prosess upload sudah selesai
    myDropzone.on("queuecomplete", function (progress) {
        document.querySelector("#total-progress").style.opacity = "0";
    });

    myDropzone.on("complete", function (file, progress) {
        document.querySelector("#total-progress").style.opacity = "0";
        this.removeFile(file);
    });

// Membuat fungsi mengunggah semua gambar pada tombol start
    document.querySelector("#actions .start").onclick = function () {
        myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
    };
// Membuat fungsi pembatalan semua gambar pada saat upload
    document.querySelector("#actions .cancel").onclick = function (file) {
        myDropzone.fileupload - process(true);
    };

    document.querySelector("button.removeAll").onclick = function () {
        myDropzone.removeAllFiles();
    };
})