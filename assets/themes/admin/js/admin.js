$(function () {
    //    FULLCALENDAR
    var calendar = $('#calendar').fullCalendar({
        lang: 'id',
        themeSystem: 'bootstrap4',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,listMonth'
        },
        timezone: 'local',
        eventLimit: true, // allow "more" link when too many events
        events: base_url + 'admin/agenda/get_all_event/',
        eventClick: function (event, jsEvent, view) {
            $("#show_judul_agenda").val(event.title);
            $("#show_tautan_agenda").val(event.tautan);
            $('#show_tgl_mulai_agenda').val(moment(event.start).format('DD MMM YYYY, HH:mm') + ' WIB');
            $('#show_tgl_berakhir_agenda').val(moment(event.end).format('DD MMM YYYY, HH:mm') + ' WIB');
            $("#show_warna_bg").val(event.color);
            $("#show_warna_text").val(event.textColor);
            $('#editModal').modal();

            $("form#updateEvent").attr('action', base_url + 'admin/agenda/update_event/' + event.id + '');
        },
    });

    $("#delete_agenda").on('click', function () {
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var href = $("#updateEvent").attr('action');
                var id = href.split('/')[7]; //Mengambil ID user hasil dari split url

                $.ajax({
                    url: base_url + 'admin/agenda/delete_event/' + id,
                    success: function (data) {
                        $('#editModal').modal('hide');

                        calendar.fullCalendar("refetchEvents");
                    }
                });
            }
        });
    });

    $("#addEvent").on('submit', function (event) {
        event.preventDefault();

        var form = $(this).serialize();

        $.ajax({
            type: "POST",
            url: base_url + 'admin/agenda/add_event',
            data: form,
            success: function (data) {
                calendar.fullCalendar("refetchEvents");
            },
        });
    });

    $("#updateEvent").on('submit', function (event) {
        event.preventDefault();

        var form = $(this).serialize();

        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: form,
            success: function (data) {
                console.log(data);

                $('#editModal').modal('hide');

                calendar.fullCalendar("refetchEvents");
            },
        });
    });

    $('#tgl_mulai input').datetimepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '2018:2100', //set the range of years
        dateFormat: 'dd-mm-yy', //set the format of the date
        timeFormat: 'HH:mm:ss'
    }).val();

    $('#tgl_berakhir input').datetimepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '2018:2100', //set the range of years
        dateFormat: 'dd-mm-yy', //set the format of the date
        timeFormat: 'HH:mm'
    }).val();

    $('#start_date input').val($('#start_date input').data('date'));
    $('#start_date input').datetimepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '2018:2100', //set the range of years
        dateFormat: 'yy-mm-dd', //set the format of the date
        timeFormat: 'HH:mm:ss',
    }).val();

    $('#end_date input').val($('#end_date input').data('date'));
    $('#end_date input').datetimepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '2018:2100', //set the range of years
        dateFormat: 'yy-mm-dd', //set the format of the date
        timeFormat: 'HH:mm:ss',
    }).val();

    // DATATABLE SISWA
    var table = $('#datatable').DataTable({
        responsive: true
    });


    // MODAL INFO
    $(document).on('show.bs.modal', '#modal_info_gallery', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id_photo = button.data('id');

        $.ajax({
            url: base_url + 'admin/galeri/get_where/' + id_photo,
            success: function (data) {
                $("#preview_gambar").attr('src', base_url + 'uploads/img/galery/' + data.filename + '');
                $("#info_kategori").val(data.categories);
                $("#info_keterangan").val(data.content);
                $("#info_penulis").val(data.created_by);
                $("#info_tanggal").val(data.date);

                $("form").attr('action', base_url + 'admin/galeri/update_post/' + id_photo + '');
            }
        });
    });

    $(document).on('show.bs.modal', '#modal_info_guru', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id_photo = button.data('id');

        $.ajax({
            url: base_url + 'admin/guru/get_where/' + id_photo,
            success: function (data) {
                console.log(data);
                $("#preview_gambar").attr('src', base_url + 'uploads/img/guru/' + data.image + '');
                $("#nama_guru").val(data.name);
                $("#nama_profesi").val(data.profession);
                $("#tahun").val(data.year);
                $("#email").val(data.email);
                $("#info_penulis").val(data.created_by);
                $("#info_tanggal").val(data.date);

                $("form").attr('action', base_url + 'admin/guru/update_post/' + id_photo + '');
            }
        });
    });

    $(document).on('show.bs.modal', '#modal_info_fasilitas', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id_photo = button.data('id');

        $.ajax({
            url: base_url + 'admin/fasilitas/get_where/' + id_photo,
            success: function (data) {
                console.log(data);
                $("#preview_gambar").attr('src', base_url + 'uploads/img/fasilitas/' + data.image + '');
                $("#fasilitas_nama").val(data.name);
                $("#fasilitas_keterangan").val(data.content);
                $("#info_penulis").val(data.created_by);
                $("#info_tanggal").val(data.date);

                $("form").attr('action', base_url + 'admin/fasilitas/update_post/' + id_photo + '');
            }
        });
    });

    $(document).on('show.bs.modal', '#modal_info_jurusan', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id = button.data('id')
        kelas = button.data('class');

        $.ajax({
            url: base_url + 'admin/program_keahlian/get_info_jurusan/' + id,
            success: function (data) {
                console.log(data);
                $("#info_jurusan").val(data.name);
                $("textarea#konten_blog").froalaEditor('html.set', data.content);
                $("#info_sejak").val(data.since);
                $("#info_tanggal").val(data.date);

                $("form").attr('action', base_url + 'admin/program_keahlian/update_info_jurusan/' + kelas + '/' + id + '');
            }
        });
    });

    $(document).on('show.bs.modal', '#modal_info_gambar', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id_photo = button.data('id'),
            kelas = button.data('class'),
            kelas_id = button.data('class_id');

        $.ajax({
            url: base_url + 'admin/program_keahlian/get_image/' + id_photo,
            success: function (data) {
                $("#preview_gambar").attr('src', base_url + 'uploads/img/jurusan/' + data.image + '');
                $("#info_tanggal").val(data.date);

                $("form").attr('action', base_url + 'admin/program_keahlian/update_post/' + kelas + '/' + id_photo + '/' + kelas_id);
            }
        });
    });

    $(document).on('show.bs.modal', '#infoLink', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id = button.data('id');

        $.ajax({
            url: base_url + 'admin/link/get/' + id,
            success: function (data) {
                $("#title_link").val(data.title);
                $("#url_link").val(data.url);

                $("form#edit").attr('action', base_url + 'admin/link/add/' + id);
            }
        });
    });

    $(document).on('show.bs.modal', '#infoAlumni', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id = button.data('id');

        $.ajax({
            url: base_url + 'admin/alumni/get/' + id,
            success: function (data) {
                $("form#edit").attr('action', base_url + 'admin/alumni/add/' + id);
                $("#info_nama").val(data.name);
                $("#info_lulusan").val(data.graduate);
                $("#info_kuliah").val(data.college);
                $("#info_pekerjaan").val(data.work);
                $("#info_telp").val(data.phone_number);
                $("#info_email").val(data.email);
                $("#info_alamat").val(data.address);
                $("#info_pesan").val(data.message);
                $("select#info_status").val(data.status);
                $('select#info_status option[value=data.status]').prop('selected', true);
            }
        });
    });


    // DELETE
    $(document).on('click', '#delete_blog', function () {
        var slug = $(this).data('slug');
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/blog/delete_post/' + slug);
            }
        });
    });

    $(document).on('click', '#delete_pengumuman', function () {
        var slug = $(this).data('slug');
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/pengumuman/delete_post/' + slug);
            }
        });
    });

    $(document).on('click', '#delete_galery', function () {
        var id = $(this).data('id');
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/galeri/delete_post/' + id);
            }
        });
    });

    $(document).on('click', '#delete_guru', function () {
        var id = $(this).data('id');
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/guru/delete_post/' + id);
            }
        });
    });

    $(document).on('click', '#delete_fasilitas', function () {
        var id = $(this).data('id');
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/fasilitas/delete_post/' + id);
            }
        });
    });

    $(document).on('click', '#delete_info_jurusan', function () {
        var id = $(this).data('id')
        kelas = $(this).data('class');
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/program_keahlian/delete_jurusan/' + kelas + '/' + id);
            }
        });
    });

    $(document).on('click', '#delete_gambar_jurusan', function () {
        var id = $(this).data('id')
        class_name = $(this).data('class')
        class_id = $(this).data('class_id');
        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/program_keahlian/delete_post/' + class_name + '/' + id + '/' + class_id);
            }
        });
    });

    $(document).on('click', '#delete_tautan', function () {
        var id = $(this).data('id');

        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/link/delete/' + id);
            }
        });
    });

    $(document).on('click', '#delete_alumni', function () {
        var id = $(this).data('id');

        swal({
            title: 'Anda yakin untuk menghapus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(base_url + 'admin/alumni/delete/' + id);
            }
        });
    });


    $('#konten_blog').froalaEditor({
            toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|',
                'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align',
                'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertImage',
                'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll',
                'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo'
            ],
            // FILE UPLOAD
            fileUploadURL: base_url + 'admin/blog/froala_file/',
            fileUploadParams: {
                id: 'my_editor'
            },

            // IMAGE UPLOAD & MANAGER
            imageUploadURL: base_url + 'admin/blog/froala_uploads/',
            imageDeleteURL: base_url + 'admin/blog/froala_delete/',
            imageManagerLoadURL: base_url + 'admin/blog/froala_manager/',
            imageManagerDeleteURL: base_url + 'admin/blog/froala_delete/',
            imageAllowedTypes: ['jpeg', 'jpg', 'png'],

            // VIDEO UPLOAD
            videoUploadParam: 'video_param',
            videoUploadURL: base_url + 'blog/froala_video/',
            videoUploadParams: {
                id: 'my_editor'
            },
            videoUploadMethod: 'POST',
            videoMaxSize: 50 * 1024 * 1024,
            videoAllowedTypes: ['webm', 'jpg', 'ogg', '3gp', 'mp4', 'avi', 'mkv'],

            height: 500,
        })
        .on('editable.afterRemoveImage', function (e, editor, $img) {
            editor.options.imageDeleteParams = {
                src: $img.attr('src')
            };
            editor.deleteImage($img);
        });

    $('#konten_blog').froalaEditor('html.get');

    $('#kategori_blog').tagsInput({
        defaultText: '',
        height: '',
        width: '',
    });

    $('#tags_blog').tagsInput({
        defaultText: '',
        height: '',
        width: '',
    });

    function readURLs(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(".cover-blog").attr('style', 'background-image:url(' + e.target.result + ')');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview_gambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#input_gambar').change(function () {
        readURL(this);
    });

    $('#post_image').change(function () {
        readURLs(this);
    });
});