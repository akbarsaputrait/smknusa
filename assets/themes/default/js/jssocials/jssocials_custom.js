$(document).ready(function () {
    $("#sharePost").jsSocials({
        url: $("#link_post").attr("href"),
        text: $("h1").data("title"),
        showCount: false,
        showLabel: false,
        shareIn: 'blank',
        shares: [
            {
                share: "facebook",
                logo: "fa fa-facebook"
            },
            {
                share: "messenger",
                label: "Share",
                logo: "fa fa-comment",
            }
            ,
            {
                share: "twitter",
                logo: "fa fa-twitter"
            },
            {
                share: "whatsapp",
                logo: "fa fa-whatsapp"
            }
        ]
    });
});