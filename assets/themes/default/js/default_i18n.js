/**
 * Default theme functions
 */
$(document).ready(function () {
    $('nav.navigation-bar').coreNavigation({
        menuPosition: "right",
        container: true,
        dropdownEvent: 'click',
        mode: 'fixed'
    });

    // FASILITAS
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
    });

    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        centerMode: true,
        focusOnSelect: true,
    });


    // HEADER
    $('.single-item').slick({
        dots: true,
        arrows: true,
        fade: true,
        autoplay: true,
        autoplaySpeed: 2500,
    });


    // JURUSAN
    $('.slider-nama-jur').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        nextArrow: '<button type="button" class="slick-next" style="color: #333;">Next</button>',
        prevArrow: '<button type="button" class="slick-prev" style="color: #333;">Prev</button>',
        asNavFor: '.slider-jurusan',
        autoplay: true,
        autoplaySpeed: 3000,
    });

    $('.slider-jurusan').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        draggable: false,
        fade: true,
        focusOnSelect: true
    });
});
