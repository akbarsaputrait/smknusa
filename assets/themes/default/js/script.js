$(document).ready(function () {
    var table = $('#datatable').DataTable({
        responsive: true,
    });

    $(document).on('show.bs.modal', '#infoAlumni', function (event) {
        var button = $(event.relatedTarget),
            $modal = $(this),
            id = button.data('id');

        $.ajax({
            url: config.baseURL + 'profile/get_alumni/' + id,
            success: function (data) {
                $("#info_nama").val(data.name);
                $("#info_lulusan").val(data.graduate);
                $("#info_kuliah").val(data.college);
                $("#info_pekerjaan").val(data.work);
                $("#info_telp").val(data.phone_number);
                $("#info_email").val(data.email);
                $("#info_alamat").val(data.address);
                $('#info_pesan').val(data.message);

            }
        });
    });
});