$(document).ready(function () {
    //    FULLCALENDAR
    var calendar = $('#calendar').fullCalendar({
        lang: 'id',
        themeSystem: 'bootstrap4',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,listMonth'
        },
        timezone: 'local',
        eventLimit: true, // allow "more" link when too many events
        events: config.baseURL + 'event/get_all_event/',
        eventClick: function (event, jsEvent, view) {
            $("#show_judul_agenda").val(event.title);
            $("#show_tautan_agenda").val(event.tautan);
            $('#show_tgl_mulai_agenda').val(moment(event.start).format('DD MMM YYYY, HH:mm') + ' WIB');
            $('#show_tgl_berakhir_agenda').val(moment(event.end).format('DD MMM YYYY, HH:mm') + ' WIB');
            $("#show_warna_bg").val(event.color);
            $("#show_warna_text").val(event.textColor);
            $('#editModal').modal();
        },
    });
})