<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Default Public Template
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
    <link rel="icon" type="image/x-icon" href="/favicon.ico?v=<?php echo $this->settings->site_version; ?>">
    <title><?php echo $page_title; ?> - <?php echo $this->settings->site_name; ?></title>
    <meta name="keywords" content="<?php echo $this->settings->meta_keywords; ?>">
    <meta name="description" content="<?php echo $this->settings->meta_description; ?>">

    <?php // CSS files ?>
    <?php if (isset($css_files) && is_array($css_files)): ?>
        <?php foreach ($css_files as $css): ?>
            <?php if (!is_null($css)): ?>
                <?php $separator = (strstr($css, '?')) ? '&' : '?'; ?>
                <link rel="stylesheet"
                      href="<?php echo $css; ?><?php echo $separator; ?>v=<?php echo $this->settings->site_version; ?>"><?php echo "\n"; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div id="load" class="d-flex justify-content-center align-items-center w-100 bg-white position-fixed"
     style="z-index: 100;">
    <img src="<?= base_url() ?>assets/themes/default/img/smk.png" width="100" class="animated infinite zoomIn">
</div>


<?php // Main body ?>
<div role="main">
    <nav hidden class="navigation-bar">
        <div class="nav-header">
            <a href="<?= base_url(); ?>" class="brand">
                <img src="<?= base_url() . 'assets/themes/default/img/logo_smk_black.png' ?>"/>
            </a>
            <button class="toggle-bar">
                <span class="fa fa-bars"></span>
            </button>
        </div>
        <ul class="menu">
            <li><a href="<?= base_url(); ?>">Beranda</a></li>
            <li class="megamenu">
                <a href="#">Tentang</a>
                <div class="megamenu-content">
                    <div class="row p-3 mr-0">
                        <div class="col-md-3 pb-5">
                            <a href="<?= base_url('profile') ?>" style="text-decoration: none;"
                               class="m-0 p-0">
                                <h6 class="ul-title">
                                    <strong>Profil Sekolah</strong>
                                </h6>
                            </a>
                            <ul class="pl-3 list-margin">
                                <li class="float-none">
                                    <a href="#" class="p-0">Struktur Organisasi</a>
                                </li>
                                <li class="float-none">
                                    <a href="<?= base_url('profile/kemitraan') ?>" class="p-0">Kemitraan</a>
                                </li>
                                <li class="float-none">
                                    <a href="<?= base_url('profile/program_kerja') ?>" class="p-0">Program Kerja</a>
                                </li>
                                <li class="float-none">
                                    <a href="#" class="p-0">Kondisi Siswa</a>
                                </li>
                                <li class="float-none">
                                    <a href="<?= base_url('profile/komite_sekolah') ?>" class="p-0">Komite Sekolah</a>
                                </li>
                                <li class="float-none">
                                    <a href="#" class="p-0">Peserta</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6 pb-5">
                            <a href="<?= base_url() ?>" style="text-decoration: none;"
                               class="m-0 p-0">
                                <h6 class="ul-title">
                                    <strong>Jurusan</strong>
                                </h6>
                            </a>
                            <div class="row pl-3">
                                <div class="col-6">
                                    <h6>
                                        <strong>Teknik Pertanian</strong>
                                    </h6>
                                    <ul class="pl-3 list-margin">
                                        <?php
                                        foreach ($pertanian as $item):
                                            ?>
                                            <li class="float-none">
                                                <a href="<?= base_url() . 'jurusan/' . $item->type . '/' . $item->id . '-' . url_title($item->name, '-', TRUE) ?>"
                                                   class="p-0">
                                                    <?= $item->name; ?>
                                                </a>
                                            </li>
                                        <?php
                                        endforeach;
                                        ?>
                                    </ul>
                                </div>
                                <div class="col-6">
                                    <h6>
                                        <strong>Teknik Informatika</strong>
                                    </h6>
                                    <ul class="pl-3 list-margin">
                                        <?php
                                        foreach ($informatika as $item):
                                            ?>
                                            <li class="float-none">
                                                <a href="<?= base_url() . 'jurusan/' . $item->type . '/' . $item->id . '-' . url_title($item->name, '-', TRUE) ?>"
                                                   class="p-0">
                                                    <?= $item->name; ?>
                                                </a>
                                            </li>
                                        <?php
                                        endforeach;
                                        ?>
                                    </ul>
                                </div>
                                <div class="col-6">
                                    <h6>
                                        <strong>Teknik Mesin</strong>
                                    </h6>
                                    <ul class="pl-3 list-margin">
                                        <?php
                                        foreach ($mesin as $item):
                                            ?>
                                            <li class="float-none">
                                                <a href="<?= base_url() . 'jurusan/' . $item->type . '/' . $item->id . '-' . url_title($item->name, '-', TRUE) ?>"
                                                   class="p-0">
                                                    <?= $item->name; ?>
                                                </a>
                                            </li>
                                        <?php
                                        endforeach;
                                        ?>
                                    </ul>
                                </div>
                                <div class="col-6">
                                    <h6>
                                        <strong>Teknik Elektro</strong>
                                    </h6>
                                    <ul class="pl-3 list-margin">
                                        <?php
                                        foreach ($elektro as $item):
                                            ?>
                                            <li class="float-none">
                                                <a href="<?= base_url() . 'jurusan/' . $item->type . '/' . $item->id . '-' . url_title($item->name, '-', TRUE) ?>"
                                                   class="p-0">
                                                    <?= $item->name; ?>
                                                </a>
                                            </li>
                                        <?php
                                        endforeach;
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="<?= base_url('') ?>" style="text-decoration: none;"
                               class="m-0 p-0">
                                <h6 class="ul-title">
                                    <strong>Lainnya</strong>
                                </h6>
                            </a>

                            <ul class="pl-3 list-margin">
                                <li class="float-none">
                                    <a href="#" class="p-0">Adiwiyata</a>
                                </li>
                                <li class="float-none">
                                    <a href="#" class="p-0">E-Learning</a>
                                </li>
                                <li class="float-none">
                                    <a href="<?= base_url() . 'galeri' ?>" class="p-0">Galeri</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a href="#">BKK</a>
                <ul class="dropdown-menu">
                    <li><a href="http://bkk.smkn1purwosari.sch.id/" target="_blank">Info BKK</a></li>
                    <li><a href="<?= base_url('profile/alumni') ?>">Alumni</a></li>
                </ul>
            </li>
            <li><a href="<?= base_url() . 'posts/list/blog'; ?>">Blog</a></li>
        </ul>
    </nav>
    <?php // Main content ?>
    <?php echo $content; ?>

    <section class="bg-dark text-white">
        <div class="container">
            <div class="row pb-4">
                <div class="col-md-6 mt-4">
                    <h4>
                        <b>Kontak Kami</b>
                    </h4>
                    <div style="font-size: 15px;line-height:25px;">
                        <p>0343-613747
                            <br> purwosarismkn1@yahoo.co.id
                            <br> Jalan Raya Purwosari, Pasuruan Jawa Timur</p>
                    </div>
                    <i class="fa fa-instagram"></i>
                    <i class="fa fa-facebook fb"></i>
                </div>
                <div class="col-md-6 mt-4">
                    <h4>
                        <b>Lainnya</b>
                    </h4>
                    <div class="w-100">
                        <?php
                        foreach ($links as $item):
                            ?>
                            <a href="<?= $item->url; ?>" class="btn btn-outline-light"
                               target="_blank"><?= $item->title; ?></a>
                        <?php
                        endforeach;
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>
<?php // Javascript files ?>
<?php if (isset($js_files) && is_array($js_files)): ?>
    <?php foreach ($js_files as $js): ?>
        <?php if (!is_null($js)): ?>
            <?php $separator = (strstr($js, '?')) ? '&' : '?'; ?>
            <?php echo "\n"; ?>
            <script type="text/javascript"
                    src="<?php echo $js; ?><?php echo $separator; ?>v=<?php echo $this->settings->site_version; ?>"></script><?php echo "\n"; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>
<?php if (isset($js_files_i18n) && is_array($js_files_i18n)): ?>
    <?php foreach ($js_files_i18n as $js): ?>
        <?php if (!is_null($js)): ?>
            <?php echo "\n"; ?>
            <script type="text/javascript"><?php echo "\n" . $js . "\n"; ?></script><?php echo "\n"; ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>

<script>
    $(document).ready(function () {
        setTimeout(function () {
            $("#load").fadeOut(1000, function () {
                $("#load").addClass("d-none");
                $("#load").removeClass("d-flex");
            });
        }, 2500);
    });
</script>

</body>
</html>
