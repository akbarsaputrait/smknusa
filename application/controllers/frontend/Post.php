<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/8/2018
 * Time: 6:29 PM
 */

class Post extends Public_Controller
{
    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('welcome');
        $this->load->model('guru_model', 'guru');
        $this->load->model('post_model', 'post');
    }

    public function index($type, $slug)
    {
        // GET ID FROM SLUG OF CONTENT
        $array_slug = explode('-', $slug);
        $id = $array_slug[0];

        // GET DATA OF CONTENT
        $post = $this->post->get_where($type, $id);
        
        if(is_null($post)) {
            redirect('404_override');
        } else {
            // setup page header data
            $this->set_title($post[0]->title)
                ->add_css_theme('jssocials/jssocials-theme-flat.css')
                ->add_js_theme(array(
                    'jssocials/jssocials.min.js',
                    'jssocials/jssocials_custom.js'
                ));

            $data = $this->includes;

            // set content data
            $content_data = array(
                'post' => $post[0],
                'color' => ($type == "pengumuman") ? "yellow-content" : "green-content",
                'informatika' => $this->keahlian->get("informatika"),
                'mesin' => $this->keahlian->get("mesin"),
                'pertanian' => $this->keahlian->get("pertanian"),
                'elektro' => $this->keahlian->get("elektro"),
                'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
                'agenda' => $this->agenda->get_agenda(3),
                'posts' => $this->post->get("blog", 3, null, null, true, false),
                'links' => $this->link->get()
            );

            // load views
            $data['content'] = $this->load->view('post', $content_data, TRUE);
            $this->load->view($this->template, $data);
        }
    }


    public function list($type)
    {
        // GET DATA OF CONTENT
        $post = $this->post->get();

        // setup page header data
        $this->set_title("Daftar " . $type);

        $data = $this->includes;

        $limit = $this->settings->per_page_limit;

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'posts/list/' . $type;
        $config['total_rows'] = $this->post->count_all($type);
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $config['page_query_string'] = "offset";

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';


        $this->pagination->initialize($config);

        // set content data
        $content_data = array(
            'title' => "Daftar " . $type,
            'post' => $this->post->get($type, $limit, $this->uri->segment(4), true, false, false),
            'color' => ($type == "pengumuman") ? "yellow-content" : "green-content",
            'pagination' => $this->pagination->create_links(),
            'posts' => $this->post->get("blog", 3, null, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'agenda' => $this->agenda->get_agenda(3),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),

        );

        // load views
        if ($type == "pengumuman") {
            $data['content'] = $this->load->view('list_news', $content_data, TRUE);
        } else {
            $data['content'] = $this->load->view('list_blog', $content_data, TRUE);
        }
        $this->load->view($this->template, $data);
    }
}