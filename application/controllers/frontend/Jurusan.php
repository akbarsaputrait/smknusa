<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/11/2018
 * Time: 1:28 PM
 */

class Jurusan extends Public_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index($type, $slug)
    {
        // GET ID FROM SLUG OF CONTENT
        $array_slug = explode('-', $slug);
        $id = $array_slug[0];

        $class = $this->keahlian->get($type, $id);

        $color = NULL;

        if ($type == "informatika" || $type == "elektro") {
            $color = "yellow-content";
        } elseif ($type == "mesin") {
            $color = "blue-content";
        } else {
            $color = "green-content";
        }


        // setup page header data
        $this->set_title($class[0]->name)
            ->add_css_theme('jssocials/jssocials-theme-flat.css')
            ->add_js_theme(array(
                'jssocials/jssocials.min.js',
                'jssocials/jssocials_custom.js',
                'script.js'
            ));;

        $data = $this->includes;

        // set content data
        $content_data = array(
            'class' => $class,
            'color' => $color,
            'image' => $this->keahlian->get_image($class[0]->id),
            'post' => $this->post->get('blog', 3, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'links' => $this->link->get()
        );

        // load views
        $data['content'] = $this->load->view('jurusan', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
}