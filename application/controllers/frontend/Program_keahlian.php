<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/11/2018
 * Time: 1:28 PM
 */

class Program_keahlian extends Public_Controller
{
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->load->model('keahlian_model', 'keahlian');
    }

    public function jurusan($type, $id)
    {
        // setup page header data
        $this->set_title("Nama Jurusan");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'keahlian' => $this->keahlian->get($type, $id),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'posts' => $this->post->get("blog", 3, null, true, true),
            'links' => $this->link->get()
        );

        // load views
        $data['content'] = $this->load->view('jurusan', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
}