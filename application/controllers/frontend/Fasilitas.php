<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/29/2018
 * Time: 10:58 AM
 */

class Fasilitas extends Public_Controller
{
    function index()
    {
        // setup page header data
        $this->set_title('Fasilitas')
            ->add_css_theme(array(
                'lightgallery/css/lightgallery.min.css',
                'lightgallery/css/lg-transitions.min.css'
            ))
            ->add_js_theme(array(
                'lightgallery/lightgallery-all.min.js',
                'lightgallery/custom.js'
            ));

        $data = $this->includes;

        $limit = $this->settings->per_page_limit;

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'fasilitas';
        $config['total_rows'] = $this->fasilitas->count_all();
        $config['per_page'] = $limit;
        $config['uri_segment'] = 2;
        $config['page_query_string'] = "offset";

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';


        $this->pagination->initialize($config);

        // set content data
        $content_data = array(
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'posts' => $this->post->get('blog', 3, null, true, false),
            'fasilitas' => $this->fasilitas->get(null, $limit, $this->uri->segment(2)),
            'pagination' => $this->pagination->create_links(),
            'links' => $this->link->get(),
            'color' => "green-content",
            'title' => 'Fasilitas'
        );

        // load views
        $data['content'] = $this->load->view('fasilitas', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
}