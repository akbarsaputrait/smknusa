<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/13/2018
 * Time: 6:36 AM
 */

class Profile extends Public_Controller
{
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('welcome');

    }

    public function index()
    {
        // setup page header data
        $this->set_title("Profil Sekolah");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'title' => "Profile Sekolah",
            'color' => 'green-content',
            'post' => $this->post->get("blog", 3, null, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'fasilitas' => $this->fasilitas->get(null, null),
            'links' => $this->link->get()
        );

        // load views
        $data['content'] = $this->load->view('profile', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    public function alumni()
    {
        // setup page header data
        $this->set_title("Direktori Alumni")
            ->add_css_theme(array(
                "datatables/responsive.bootstrap4.min.css",
                "datatables/dataTables.bootstrap4.min.css",
                "datatables/fixedHeader.bootstrap4.min.css",
                "datatables/buttons.bootstrap4.min.css",
            ))
            ->add_js_theme(array(
                "datatables/jquery.dataTables.min.js",
                "datatables/dataTables.responsive.min.js",
                "datatables/responsive.bootstrap4.js",
                "datatables/dataTables.bootstrap4.min.js",
                "datatables/dataTables.fixedHeader.min.js"
            ));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'title' => "Direktori Alumni",
            'color' => 'blue-content',
            'post' => $this->post->get("blog", 3, null, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'alumni' => $this->alumni->get("publish", null, true),
            'links' => $this->link->get()

        );

        // load views
        $data['content'] = $this->load->view('alumni', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    public function program_kerja()
    {
        // setup page header data
        $this->set_title("Program Kerja");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'title' => "Program Kerja",
            'color' => 'yellow-content',
            'post' => $this->post->get("blog", 3, null, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'links' => $this->link->get()

        );

        // load views
        $data['content'] = $this->load->view('program_kerja', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    public function komite_sekolah()
    {
        // setup page header data
        $this->set_title("Komite Sekolah");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'title' => "Komite Sekolah",
            'color' => 'yellow-content',
            'post' => $this->post->get("blog", 3, null, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'links' => $this->link->get()

        );

        // load views
        $data['content'] = $this->load->view('komite_sekolah', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    public function kemitraan()
    {
        // setup page header data
        $this->set_title("Kemitraan");

        $data = $this->includes;

        // set content data
        $content_data = array(
            'title' => "Kemitraan",
            'color' => 'green-content',
            'post' => $this->post->get("blog", 3, null, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'links' => $this->link->get()

        );

        // load views
        $data['content'] = $this->load->view('kemitraan', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    public function get_alumni($id)
    {
        header('Content-Type: application/json');

        $data = array();

        $query = $this->alumni->get(null, $id);

        foreach ($query as $item) {
            $data = array(
                'name' => $item->name,
                'graduate' => $item->graduate,
                'college' => $item->college,
                'work' => $item->work,
                'address' => $item->address,
                'phone_number' => $item->phone_number,
                'email' => $item->email,
                'message' => $item->message,
            );
        }

        echo json_encode($data);
    }

    public function add_alumni()
    {
        $this->form_validation->set_rules('name', 'Nama', 'required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('work', 'Pekerjaan', 'required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');


        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('failed', validation_errors());

            redirect('profile/alumni');
        } else {

            $data = array(
                'name' => $this->input->post('name'),
                'graduate' => $this->input->post('graduate'),
                'college' => $this->input->post('college'),
                'work' => $this->input->post('work'),
                'address' => $this->input->post('address'),
                'phone_number' => $this->input->post('phone_jumber'),
                'email' => $this->input->post('email'),
                'message' => $this->input->post('message'),
                'status' => "draft",
                'is_deleted' => 0,
            );

            $query = $this->db->insert('alumni', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success">Berhasil, terima kasih telah mendaftar. Data anda akan segera terverifikasi oleh Admin.</div>');
                redirect('profile/alumni');
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger">Data anda gagal ditambahkan. Silahkan coba lagi.</div>');
                redirect('profile/alumni');
            }
        }
    }
}