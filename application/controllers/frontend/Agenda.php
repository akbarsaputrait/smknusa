<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agenda extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

//	VIEW
    public function index()
    {
        // setup page header data
        $this
            ->set_title("Agenda")
            ->add_css_theme(array(
                "fullcalendar/fullcalendar.css",
                "blog/frontend.css"
            ))
            ->add_js_theme(array(
                "fullcalendar/moment.min.js",
                "fullcalendar/fullcalendar.js",
                "fullcalendar/locale/id.js",
                "calendar.js"
            ));

        $data = $this->includes;

        $content_data = array(
            'color' => 'yellow-content',
            'title' => "Agenda",
            'posts' => $this->post->get("blog", 3, null, null, true, false),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'links' => $this->link->get()
        );

        // load views
        $data['content'] = $this->load->view('agenda', $content_data, true);

        $this->load->view($this->template, $data);
    }

//	DATA
    public function get_all_event()
    {
        header('Content-Type: application/json');

        $data = array();

        $query = $this->agenda->get_agenda();

        foreach ($query as $item) {
            $data[] = array(
                'id' => $item->id,
                'title' => $item->title,
                'url' => $item->url,
                'start' => $item->start_date,
                'end' => $item->end_date,
                'color' => $item->background_color,
                'textColor' => $item->text_color,
            );
        }

        echo json_encode($data);
    }

}

