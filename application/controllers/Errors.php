<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends Public_Controller {

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // disable the profiler
        $this->output->enable_profiler(FALSE);
    }


    /**
     * Custom 404 page
     */
    function error404()
    {
        // setup page header data
        $this->set_title(lang('core error page_not_found'));

        $data = $this->includes;

        $content_data = array(
            'guru' => $this->guru->get(null, null, null, true),
            'post' => $this->post->get('blog', 6, null, true, false, false),
            'posts' => $this->post->get('blog', 5, null, true, true, false),
            'fasilitas' => $this->fasilitas->get(null, null),
            'jurusan' => $this->keahlian->get(null, null, true),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, true),
            'pengumumans' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'links' => $this->link->get()
        );

        // load views
        $data['content'] = $this->load->view("errors/error_404", $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

}
