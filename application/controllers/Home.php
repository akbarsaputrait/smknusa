<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller
{

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('welcome');
    }


    /**
     * Default
     */
    function index()
    {
        // setup page header data
        $this->set_title(sprintf(lang('welcome title'), $this->settings->site_name));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'guru' => $this->guru->get(null, null, null, true),
            'post' => $this->post->get('blog', 6, null, true, false, false),
            'posts' => $this->post->get('blog', 5, null, true, true, false),
            'fasilitas' => $this->fasilitas->get(null, null),
            'jurusan' => $this->keahlian->get(null, null, true),
            'informatika' => $this->keahlian->get("informatika"),
            'mesin' => $this->keahlian->get("mesin"),
            'pertanian' => $this->keahlian->get("pertanian"),
            'elektro' => $this->keahlian->get("elektro"),
            'pengumuman' => $this->post->get('pengumuman', 4, null, true, false, true),
            'pengumumans' => $this->post->get('pengumuman', 4, null, true, false, false),
            'agenda' => $this->agenda->get_agenda(3),
            'links' => $this->link->get()
        );

        // load views
        $data['content'] = $this->load->view('home', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
}
