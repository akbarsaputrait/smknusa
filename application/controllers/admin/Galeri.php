<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Galeri extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('galeri_model', 'galeri');

    }

    // VIEW UNTUK GALERI
    public function index()
    {
        // setup page header data
        $this
            ->set_title("Daftar Galeri");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Galeri / Daftar Galeri',
            'galery' => $this->galeri->get()
        );

        // load views
        $data['content'] = $this->load->view('admin/galeri/galeri_daftar', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function add()
    {
        // setup page header data
        $this
            ->set_title("Tambah Galeri")
            ->add_js_theme('galeri_dropzone.js', TRUE);

        $data = $this->includes;
        $content_data = array(
            'sub' => 'Galeri / Tambah Galeri',
        );

        // load views
        $data['content'] = $this->load->view('admin/galeri/galeri_tambah', $content_data, true);

        $this->load->view($this->template, $data);
    }

    // FUNCTION UNTUK GALERI
    public function get_where($id)
    {
        header('Content-Type: application/json');
        $data = array();

        $query = $this->galeri->get($id);

        foreach ($query as $key => $value) {
            $data = array(
                'id' => $value->id,
                'filename' => $value->filename,
                'categories' => $value->categories,
                'content' => $value->content,
                'date' => date('d F Y', strtotime($value->created_at)),
                'created_by' => $value->name,
            );
        }

        echo json_encode($data);
    }

    public function add_photo()
    {
        $data = array();

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $fileName = $_FILES['file']['name'];
            $fileType = $_FILES['file']['type'];
            $fileSize = $_FILES['file']['size'];
            $targetPath = './uploads/img/galery/';
            $fileName = explode(".", $_FILES['file']['name']);
            //  Get extension.
            $extension = end($fileName);
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            $targetFile = $targetPath . $name;

            move_uploaded_file($tempFile, $targetFile);

            // GET FILENAME WITHOUT EXTENSIONS
            $filenameWithoutExt = pathinfo($name, PATHINFO_FILENAME);
            // CALL IMAGE COMPRESS LIBRARY
            $compress = new Compress();
            // CONFIG COMPRESS
            $compress->file_url = base_url() . 'uploads/img/galery/' . $name;
            $compress->new_name_image = $filenameWithoutExt . '_resz';
            $compress->quality = 50;
            $compress->destination = base_url() . 'uploads/img/galery/';

            $data = array(
                'filename' => $compress->compress_image()['image'],
                'categories' => $this->input->post('kategori'),
                'content' => $this->input->post('keterangan'),
                'created_by' => $this->user['id'],
                'created_at' => date("Y-m-d H:i:s"),
                'is_deleted' => 0
            );

            $query = $this->db->insert('galery', $data);

            if ($query) {
                $data = array(
                    'status' => 200
                );
            } else {
                $data = array(
                    'status' => 400
                );
            }
        }
        echo json_encode($data);
    }

    public function update_post($id)
    {
        $config['encrypt_name'] = true;
        $config['upload_path'] = './uploads/img/galery/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '10480';
        $config['remove_space'] = true;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload("photo_galery")) {

            $data = array(
                'categories' => $this->input->post('kategori_photo'),
                'content' => $this->input->post('keterangan_photo'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->user['id'],
                'is_deleted' => 0
            );

            $this->db->where('galery.id', $id);
            $query = $this->db->update('galery', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Diperbarui!</div>');
                redirect(base_url('admin/galeri/'));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
                redirect(base_url('admin/galeri/edit/' . $id));
            }
        } else {
            $file = array('upload_data' => $this->upload->data());

            // GET FILENAME WITHOUT EXTENSIONS
            $filename = pathinfo($file['upload_data']['file_name'], PATHINFO_FILENAME);
            // CALL IMAGE COMPRESS LIBRARY
            $compress = new Compress();
            // CONFIG COMPRESS
            $compress->file_url = base_url() . 'uploads/img/cover_post/' . $file['upload_data']['file_name'];
            $compress->new_name_image = $filename . '_resz';
            $compress->quality = 50;
            $compress->destination = base_url() . 'uploads/img/cover_post/';

            $data = array(
                'filename' => $compress->compress_image()['image'],
                'categories' => $this->input->post('kategori_photo'),
                'content' => $this->input->post('keterangan_photo'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->user['id'],
                'is_deleted' => 0
            );

            $this->db->where('galery.id', $id);
            $query = $this->db->update('galery', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Diperbarui!</div>');
                redirect(base_url('admin/galeri/'));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
                redirect(base_url('admin/galeri/edit/' . $id));
            }
        }
    }

    public function delete_post($id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('galery.id', $id);
        $query = $this->db->update('galery', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Dihapus!</div>');
            redirect(base_url('admin/galeri'));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Dihapus!</div>');
            redirect(base_url('admin/galeri'));
        }
    }

}
