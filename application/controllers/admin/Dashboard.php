<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the users model
        $this->load->model('dashboard_model', 'dashboard');
    }


    /**
     * Dashboard
     */
    function index()
    {
        // var_dump($this->session->all_userdata());exit;
        // setup page header data
        $this
            ->add_js_theme('plugin/chart-js/chart.min.js', FALSE)
            ->set_title("Dashboard");

        $data = $this->includes;

        $content_data = array(
            'total_blog' => $this->dashboard->count_post(),
            'blog' => $this->dashboard->get_post("blog", 3),
            'pengumuman' => $this->dashboard->get_post("pengumuman"),
            'alumni' => $this->alumni->get('draft', null, null, true),
            'agenda' => $this->agenda->get_agenda(4),
        );

        // load views
        $data['content'] = $this->load->view('admin/dashboard', $content_data, TRUE);

        $this->load->view($this->template, $data);
    }

}
