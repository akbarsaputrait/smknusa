<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/10/2018
 * Time: 6:13 PM
 */

class Program_keahlian extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('keahlian_model', 'keahlian');

    }

    public function list($type)
    {
        // setup page header data
        $this
            ->set_title(ucfirst($type));

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Program keahlian / ' . ucfirst($type),
            'keahlian' => $this->keahlian->get($type)
        );

        // load views
        $data['content'] = $this->load->view('admin/program_keahlian/daftar_jurusan', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function image($type, $class_id)
    {
        $class = $this->keahlian->get($type, $class_id);
        $image = $this->keahlian->get_image($class_id);

        // setup page header data
        $this
            ->set_title($class[0]->name)
            ->add_js_theme("keahlian_dropzone.js");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Program keahlian / ' . $class[0]->name . ' / Gambar',
            'image' => $image,
            'class_id' => $class[0]->id,
            'class_type' => $class[0]->type
        );

        // load views
        $data['content'] = $this->load->view('admin/program_keahlian/daftar_gambar', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function get_image($id)
    {
        header('Content-Type: application/json');
        $data = array();

        $query = $this->keahlian->get_image_where($id);

        foreach ($query as $key => $value) {
            $data = array(
                'id' => $value->id,
                'image' => $value->image,
                'date' => date('d F Y', strtotime($value->created_at)),
            );
        }

        echo json_encode($data);
    }

    public function get_info_jurusan($id)
    {
        header('Content-Type: application/json');
        $data = array();

        $query = $this->keahlian->get(null, $id);

        foreach ($query as $key => $value) {
            $data = array(
                'id' => $value->id,
                'name' => $value->name,
                'content' => $value->content,
                'since' => $value->since,
                'date' => date('d F Y', strtotime($value->created_at)),
            );
        }

        echo json_encode($data);
    }

    public function add_photo($class_id)
    {
        $data = array();
        $response = array();

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $fileName = explode(".", $_FILES['file']['name']);
            //  Get extension.
            $extension = end($fileName);
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            $targetPath = './uploads/img/jurusan/';

            $targetFile = $targetPath . $name;

            move_uploaded_file($tempFile, $targetFile);

            // GET FILENAME WITHOUT EXTENSIONS
            $filenameWithoutExt = pathinfo($name, PATHINFO_FILENAME);
            // CALL IMAGE COMPRESS LIBRARY
            $compress = new Compress();
            // CONFIG COMPRESS
            $compress->file_url = base_url() . 'uploads/img/galery/' . $name;
            $compress->new_name_image = $filenameWithoutExt . '_resz';
            $compress->quality = 50;
            $compress->destination = base_url() . 'uploads/img/galery/';

            $data = array(
                'image' => $compress->compress_image()['image'],
                'class_id' => $class_id,
                'created_by' => $this->user['id'],
                'created_at' => date("Y-m-d H:i:s"),
                'is_deleted' => 0
            );

            $query = $this->db->insert('class_image', $data);

            if ($query) {
                $response = array(
                    'status' => 200,
                    'data' => $data
                );
            } else {
                $response = array(
                    'status' => 400
                );
            }
        }
        echo json_encode($response);
    }

    public function add_jurusan($type)
    {
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('sejak_tahun', 'Sejak Tauhun', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('failed', validation_errors());
            redirect(base_url('admin/program_keahlian/list/' . $type));
        } else {
            $data = array(
                'name' => $this->input->post('jurusan'),
                'content' => $this->input->post('keterangan'),
                'since' => $this->input->post('sejak_tahun'),
                'type' => $type,
                'created_at' => date("Y-m-d H:i:s"),
                'created_by' => $this->user['id'],
                'is_deleted' => 0
            );

            $query = $this->db->insert('class', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Data Berhasil Ditambahkan!</div>');
                redirect(base_url('admin/program_keahlian/list/' . $type));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Data Gagal Ditambahkan!</div>');
                redirect(base_url('admin/program_keahlian/list/' . $type));
            }
        }
    }

    public function update_post($type, $id, $class_id)
    {
        $config['encrypt_name'] = true;
        $config['upload_path'] = './uploads/img/jurusan/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '10480';
        $config['remove_space'] = true;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload("photo_class")) {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
            redirect(base_url('admin/program_keahlian/image/' . $type . '/' . $class_id));
        } else {
            $file = array('upload_data' => $this->upload->data());

            $data = array(
                'image' => $file['upload_data']['file_name'],
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->user['id'],
                'is_deleted' => 0
            );

            $this->db->where('class_image.id', $id);
            $query = $this->db->update('class_image', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Diperbarui!</div>');
                redirect(base_url('admin/program_keahlian/image/' . $type . '/' . $class_id));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
                redirect(base_url('admin/program_keahlian/image/' . $type . '/' . $class_id));
            }
        }
    }

    public function update_info_jurusan($type, $id)
    {
        $this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $this->form_validation->set_rules('sejak_tahun', 'Sejak Tauhun', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('failed', validation_errors());
            redirect(base_url('admin/program_keahlian/list/' . $type));
        } else {
            $data = array(
                'name' => $this->input->post('jurusan'),
                'content' => $this->input->post('keterangan'),
                'since' => $this->input->post('sejak_tahun'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->user['id'],
                'is_deleted' => 0
            );

            $this->db->where('class.id', $id);
            $query = $this->db->update('class', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Data Berhasil Diperbarui!</div>');
                redirect(base_url('admin/program_keahlian/list/' . $type));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Data Gagal Diperbarui!</div>');
                redirect(base_url('admin/program_keahlian/list/' . $type));
            }
        }
    }

    public function delete_post($type, $id, $class_id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('class_image.id', $id);
        $query = $this->db->update('class_image', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Dihapus!</div>');
            redirect(base_url('admin/program_keahlian/image/' . $type . '/' . $class_id));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Dihapus!</div>');
            redirect(base_url('admin/program_keahlian/image/' . $type . '/' . $class_id));
        }
    }

    public function delete_jurusan($type, $id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('class.id', $id);
        $query = $this->db->update('class', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Dihapus!</div>');
            redirect(base_url('admin/program_keahlian/list/' . $type));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Dihapus!</div>');
            redirect(base_url('admin/program_keahlian/list/' . $type));
        }
    }
}