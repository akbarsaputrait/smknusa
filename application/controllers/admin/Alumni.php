<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/17/2018
 * Time: 6:42 PM
 */

class Alumni extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // setup page header data
        $this
            ->set_title("Daftar Alumni");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Daftar Alumni',
            'alumni' => $this->alumni->get()
        );

        // load views
        $data['content'] = $this->load->view('admin/alumni', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function add($id = NULL)
    {
        $this->form_validation->set_rules('name', 'Nama', 'required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('work', 'Pekerjaan', 'required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');


        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('failed', validation_errors());

            redirect('admin/alumni');
        } else {

            if (!is_null($id)) {
                $data = array(
                    'name' => $this->input->post('name'),
                    'graduate' => $this->input->post('graduate'),
                    'college' => $this->input->post('college'),
                    'work' => $this->input->post('work'),
                    'address' => $this->input->post('address'),
                    'phone_number' => $this->input->post('phone_number'),
                    'email' => $this->input->post('email'),
                    'message' => $this->input->post('message'),
                    'status' => $this->input->post('status'),
                    'updated_by' => $this->user['id'],
                    'updated_at' => date("Y-m-d H:i:s"),
                    'created_by' => $this->user['id'],
                );

                $this->db->where('id', $id);
                $query = $this->db->update('alumni', $data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'graduate' => $this->input->post('graduate'),
                    'college' => $this->input->post('college'),
                    'work' => $this->input->post('work'),
                    'address' => $this->input->post('address'),
                    'phone_number' => $this->input->post('phone_number'),
                    'email' => $this->input->post('email'),
                    'message' => $this->input->post('message'),
                    'status' => $this->input->post('status'),
                    'created_by' => $this->user['id'],
                    'is_deleted' => 0,
                );

                $query = $this->db->insert('alumni', $data);
            }

            if ($query) {
                if (!is_null($id)) {
                    $this->session->set_flashdata('success', '<div class="alert alert-success notif">Data Alumni berhasil diperbarui!</div>');
                } else {
                    $this->session->set_flashdata('success', '<div class="alert alert-success notif">Data Alumni berhasil ditambahkan!</div>');
                }
                redirect('admin/alumni');
            } else {
                if (!is_null($id)) {
                    $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Data Alumni gagal diperbarui!</div>');
                } else {
                    $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Data Alumni gagal ditambahkan!</div>');
                }
                redirect('admin/alumni');
            }
        }
    }

    public function get($id)
    {
        header('Content-Type: application/json');

        $data = array();

        $query = $this->alumni->get(null, $id);

        foreach ($query as $item) {
            $data = array(
                'name' => $item->name,
                'graduate' => $item->graduate,
                'college' => $item->college,
                'work' => $item->work,
                'address' => $item->address,
                'phone_number' => $item->phone_number,
                'email' => $item->email,
                'message' => $item->message,
                'status' => $item->status,
            );
        }

        echo json_encode($data);
    }

    public function delete($id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('id', $id);
        $query = $this->db->update('alumni', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Data Alumni Berhasil Dihapus!</div>');
            redirect(base_url('admin/alumni'));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Data Alumni Gagal Dihapus!</div>');
            redirect(base_url('admin/alumni'));
        }
    }

}