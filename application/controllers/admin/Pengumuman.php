<?php defined('BASEPATH') or exit('No direct script access allowed');

//require 'vendor/froala/wysiwyg-editor-php-sdk/lib/FroalaEditor.php';

class Pengumuman extends Admin_Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        // load the users model
        $this->load->model('dashboard_model', 'dashboard');
        $this->load->model('post_model', 'post');

    }

    /**
     * Post View
     */
    public function index()
    {
        // setup page header data
        $this
            ->set_title("Daftar Pengumuman");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Pengumuman / Daftar Pengumuman',
            'pengumuman' => $this->post->get("pengumuman", null, null, false, false, false)
        );

        // load views
        $data['content'] = $this->load->view('admin/pengumuman/pengumuman_daftar', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function add()
    {
        // setup page header data
        $this->set_title("Tambah Pengumuman");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Pengumuman / Tambah Pengumuman'
        );

        // load views
        $data['content'] = $this->load->view('admin/pengumuman/pengumuman_tambah', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function detail($slug)
    {
        // setup page header data
        $this->set_title("Edit Pengumuman");

        $data = $this->includes;

        $id = explode("-", $slug);

        $id_post = $id[0];

        $content_data = array(
            'sub' => 'Pengumuman / Detail Pengumuman / ',
            'post' => $this->post->get_where("pengumuman", $id_post)
        );

        // load views
        $data['content'] = $this->load->view('admin/pengumuman/pengumuman_detail', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function delete_post($slug)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('post.slug', $slug);
        $query = $this->db->update('post', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Pengumuman Berhasil Dihapus!</div>');
            redirect(base_url('admin/pengumuman'));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Pengumuman Gagal Dihapus!</div>');
            redirect(base_url('admin/pengumuman'));
        }
    }
}
