<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/16/2018
 * Time: 8:48 AM
 */

class Link extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // setup page header data
        $this
            ->set_title("Link Eksternal");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Link Eksternal',
            'links' => $this->link->get()
        );

        // load views
        $data['content'] = $this->load->view('admin/link', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function add($id = NULL)
    {
        $this->form_validation->set_rules('title', 'Judul', 'required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('url', 'Tautan', 'required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');


        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('failed', validation_errors());

            redirect('admin/link');
        } else {

            if (!is_null($id)) {
                $data = array(
                    'title' => $this->input->post('title'),
                    'url' => $this->input->post('url'),
                    'updated_by' => $this->user['id'],
                    'updated_at' => date("Y-m-d H:i:s"),
                );

                $this->db->where('id', $id);
                $query = $this->db->update('link', $data);
            } else {
                $data = array(
                    'title' => $this->input->post('title'),
                    'url' => $this->input->post('url'),
                    'created_by' => $this->user['id'],
                    'is_deleted' => 0,
                );

                $query = $this->db->insert('link', $data);
            }

            if ($query) {
                if (!is_null($id)) {
                    $this->session->set_flashdata('success', '<div class="alert alert-success notif">Tautan berhasil diperbarui!</div>');
                } else {
                    $this->session->set_flashdata('success', '<div class="alert alert-success notif">Tautan berhasil ditambahkan!</div>');
                }
                redirect('admin/link');
            } else {
                if (!is_null($id)) {
                    $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Tautan gagal diperbarui!</div>');
                } else {
                    $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Tautan gagal ditambahkan!</div>');
                }
                redirect('admin/link');
            }
        }
    }

    public function get($id)
    {
        header('Content-Type: application/json');

        $data = array();

        $query = $this->link->get($id);

        foreach ($query as $item) {
            $data = array(
                'title' => $item->title,
                'url' => $item->url,
                'id' => $item->id
            );
        }

        echo json_encode($data);
    }

    public function delete($id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('id', $id);
        $query = $this->db->update('link', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Tautan Berhasil Dihapus!</div>');
            redirect(base_url('admin/link'));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Tautan Gagal Dihapus!</div>');
            redirect(base_url('admin/link'));
        }
    }
}