<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fasilitas extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fasilitas_model', 'fasilitas');

    }

    // VIEW UNTUK FASILITAS
    public function index()
    {
        // setup page header data
        $this
            ->set_title("Daftar Fasilitas");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Fasilitas / Daftar Fasilitas',
            'fasilitas' => $this->fasilitas->get()
        );

        // load views
        $data['content'] = $this->load->view('admin/fasilitas/fasilitas_daftar', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function add()
    {
        // setup page header data
        $this
            ->set_title("Tambah Fasilitas")
            ->add_js_theme('fasilitas_dropzone.js', TRUE);

        $data = $this->includes;
        $content_data = array(
            'sub' => 'Fasilitas / Tambah Fasilitas',
        );

        // load views
        $data['content'] = $this->load->view('admin/fasilitas/fasilitas_tambah', $content_data, true);

        $this->load->view($this->template, $data);
    }

    // FUNCTION UNTUK FASILITAS
    public function get_where($id)
    {
        header('Content-Type: application/json');
        $data = array();

        $query = $this->fasilitas->get($id);

        foreach ($query as $key => $value) {
            $data = array(
                'id' => $value->id,
                'image' => $value->image,
                'content' => $value->content,
                'date' => date('d F Y', strtotime($value->created_at)),
                'name' => $value->nama_fasilitas,
                'created_by' => $value->name
            );
        }

        echo json_encode($data);
    }

    public function add_photo()
    {
        $data = array();

        $this->form_validation->set_rules('nama_fasilitas', 'Nama Fasilitas', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('failed', validation_errors());

            redirect(base_url('admin/fasilitas/add'));
        } else {
            if (!empty($_FILES)) {
                $tempFile = $_FILES['file']['tmp_name'];
                $fileType = $_FILES['file']['type'];
                $fileSize = $_FILES['file']['size'];
                $targetPath = './uploads/img/fasilitas/';
                $fileName = explode(".", $_FILES['file']['name']);
                //  Get extension.
                $extension = end($fileName);
                // Generate new random name.
                $name = sha1(microtime()) . "." . $extension;

                $targetFile = $targetPath . $name;

                move_uploaded_file($tempFile, $targetFile);

                // GET FILENAME WITHOUT EXTENSIONS
                $filenameWithoutExt = pathinfo($name, PATHINFO_FILENAME);
                // CALL IMAGE COMPRESS LIBRARY
                $compress = new Compress();
                // CONFIG COMPRESS
                $compress->file_url = base_url() . 'uploads/img/fasilitas/' . $name;
                $compress->new_name_image = $filenameWithoutExt . '_resz';
                $compress->quality = 50;
                $compress->destination = base_url() . 'uploads/img/fasilitas/';

                $data = array(
                    'image' => $compress->compress_image()['image'],
                    'name' => $this->input->post('nama_fasilitas'),
                    'content' => $this->input->post('keterangan'),
                    'created_by' => $this->user['id'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'is_deleted' => 0
                );

                $query = $this->db->insert('facility', $data);

                if ($query) {
                    $data = array(
                        'status' => 200
                    );
                } else {
                    $data = array(
                        'status' => 400
                    );
                }
            }
        }
        echo json_encode($data);
    }

    public function update_post($id)
    {
        $config['encrypt_name'] = true;
        $config['upload_path'] = './uploads/img/fasilitas/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '10480';
        $config['remove_space'] = true;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload("photo_fasilitas")) {

            $data = array(
                'name' => $this->input->post('fasilitas_nama'),
                'content' => $this->input->post('fasilitas_keterangan'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->user['id'],
                'is_deleted' => 0
            );

            $this->db->where('facility.id', $id);
            $query = $this->db->update('facility', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Diperbarui!</div>');
                redirect(base_url('admin/fasilitas/'));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
                redirect(base_url('admin/fasilitas/edit/' . $id));
            }
        } else {
            $file = array('upload_data' => $this->upload->data());

            $data = array(
                'image' => $file['upload_data']['file_name'],
                'name' => $this->input->post('fasilitas_nama'),
                'content' => $this->input->post('fasilitas_keterangan'),
                'updated_at' => date("Y-m-d H:i:s"),
                'updated_by' => $this->user['id'],
                'is_deleted' => 0
            );

            $this->db->where('facility.id', $id);
            $query = $this->db->update('facility', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Diperbarui!</div>');
                redirect(base_url('admin/fasilitas/'));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
                redirect(base_url('admin/fasilitas/edit/' . $id));
            }
        }
    }

    public function delete_post($id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('facility.id', $id);
        $query = $this->db->update('facility', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Dihapus!</div>');
            redirect(base_url('admin/fasilitas'));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Dihapus!</div>');
            redirect(base_url('admin/fasilitas'));
        }
    }

}
