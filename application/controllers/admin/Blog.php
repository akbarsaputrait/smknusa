<?php defined('BASEPATH') or exit('No direct script access allowed');


class Blog extends Admin_Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        // load the users model
        $this->load->model('dashboard_model', 'dashboard');
        $this->load->model('post_model', 'post');

        require APPPATH . 'libraries/froala/wysiwyg-editor-php-sdk/lib/FroalaEditor.php';
    }

    /**
     * Post View
     */
    public function index()
    {
        // setup page header data
        $this
            ->set_title("Daftar Blog");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Blog / Daftar Blog',
            'blog' => $this->post->get("blog", null, null, false),
        );

        // load views
        $data['content'] = $this->load->view('admin/blog/blog_daftar', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function add()
    {
        // setup page header data
        $this->set_title("Tambah Blog");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Blog / Tambah Blog'
        );

        // load views
        $data['content'] = $this->load->view('admin/blog/blog_tambah', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function detail($slug)
    {
        // setup page header data
        $this->set_title("Edit Blog");

        $data = $this->includes;

        $id = explode("-", $slug);

        $id_post = $id[0];

//        var_dump($id);exit;

        $content_data = array(
            'post' => $this->post->get_where("blog", $id_post)
        );

        // load views
        $data['content'] = $this->load->view('admin/blog/blog_detail', $content_data, true);

        $this->load->view($this->template, $data);
    }

    /**
     * Post Function
     * If post have a slug, it will be update the post.
     * But if $slugg = null. it will be add a new post thanks!
     */

    public function add_post($type, $slug = null)
    {
        $this->form_validation->set_rules('judul_blog', 'Judul Blog', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('konten_blog', 'Konten Blog', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('failed', validation_errors());

            if (!is_null($slug)) {
                redirect(base_url('admin/{$type}/add'));
            } else {
                redirect(base_url('admin/{$type}/detail/{$slug}'));
            }

        } else {
            $config['encrypt_name'] = true;
            $config['upload_path'] = './uploads/img/cover_post/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '15000';
            $config['remove_space'] = true;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if (!$this->upload->do_upload("cover_post")) {

                $data = array(
                    'title' => $this->input->post('judul_blog'),
                    'content' => $this->input->post('konten_blog'),
                    'categories' => $this->input->post('kategori_blog'),
                    'tags' => $this->input->post('tags_blog'),
                    'type' => $type,
                    'slug' => url_title($this->input->post('judul_blog'), '-', TRUE),
                    'status' => $this->input->post('post_status'),
                    'start_date' => date('Y-m-d H:i:s', strtotime($this->input->post('start_date'))),
                    'end_date' => date('Y-m-d H:i:s', strtotime($this->input->post('end_date'))),
                    'created_by' => $this->user['id'],
                    'is_deleted' => 0
                );

                if (is_null($slug)) {
                    $query = $this->db->insert('post', $data);
                } else {
                    $id = explode("-", $slug);
                    $this->db->where('id', $id[0]);
                    $query = $this->db->update('post', $data);
                }

                if ($query) {
                    if (is_null($slug)) {
                        $this->session->set_flashdata('success', '<div class="alert alert-success notif">Blog Berhasil Ditambahkan!</div>');
                    } else {
                        $this->session->set_flashdata('success', '<div class="alert alert-success notif">Blog Berhasil Diperbarui!</div>');
                    }
                    redirect(base_url('admin/' . $type));
                } else {
                    if (is_null($slug)) {
                        $this->session->set_flashdata('failed', '<div class="alert alert-success notif">Blog Gagal Ditambahkan!</div>');
                    } else {
                        $this->session->set_flashdata('failed', '<div class="alert alert-success notif">Blog Gagal Diperbarui!</div>');
                    }
                    redirect(base_url('admin/' . $type));
                }
            } else {
                // GET IMAGE DETAIILS
                $file = array('upload_data' => $this->upload->data());

                // GET FILENAME WITHOUT EXTENSIONS
                $filename = pathinfo($file['upload_data']['file_name'], PATHINFO_FILENAME);
                // CALL IMAGE COMPRESS LIBRARY
                $compress = new Compress();
                // CONFIG COMPRESS
                $compress->file_url = base_url() . 'uploads/img/cover_post/' . $file['upload_data']['file_name'];
                $compress->new_name_image = $filename . '_resz';
                $compress->quality = 50;
                $compress->destination = base_url() . 'uploads/img/cover_post/';

                $data = array(
                    'title' => $this->input->post('judul_blog'),
                    'content' => $this->input->post('konten_blog'),
                    'image' => $compress->compress_image()['image'],
                    'categories' => $this->input->post('kategori_blog'),
                    'tags' => $this->input->post('tags_blog'),
                    'type' => $type,
                    'slug' => url_title($this->input->post('judul_blog'), '-', TRUE),
                    'status' => $this->input->post('post_status'),
                    'start_date' => date('Y-m-d H:i:s', strtotime($this->input->post('start_date'))),
                    'end_date' => date('Y-m-d H:i:s', strtotime($this->input->post('end_date'))),
                    'created_by' => $this->user['id'],
                    'is_deleted' => 0
                );

                if (is_null($slug)) {
                    $query = $this->db->insert('post', $data);
                } else {
                    $id = explode("-", $slug);
                    $this->db->where('id', $id[0]);
                    $query = $this->db->update('post', $data);
                }

                if ($query) {
                    if (is_null($slug)) {
                        $this->session->set_flashdata('success', '<div class="alert alert-success notif">Blog Berhasil Ditambahkan!</div>');
                    } else {
                        $this->session->set_flashdata('success', '<div class="alert alert-success notif">Blog Berhasil Diperbarui!</div>');
                    }
                    redirect(base_url('admin/' . $type));
                } else {
                    if (is_null($slug)) {
                        $this->session->set_flashdata('failed', '<div class="alert alert-success notif">Blog Gagal Ditambahkan!</div>');
                    } else {
                        $this->session->set_flashdata('failed', '<div class="alert alert-success notif">Blog Gagal Diperbarui!</div>');
                    }
                    redirect(base_url('admin/' . $type));
                }
            }
        }
    }

    public function delete_post($slug)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('post.slug', $slug);
        $query = $this->db->update('post', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Blog Berhasil Dihapus!</div>');
            redirect(base_url('admin/blog'));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Blog Gagal Dihapus!</div>');
            redirect(base_url('admin/blog'));
        }
    }

    /**
     * Froala Function
     */
    public function froala_uploads()
    {
        try {
            $tempFile = $_FILES['file']['tmp_name'];
            $fileName = explode(".", $_FILES['file']['name']);
            $targetPath = 'uploads/img/post/';

            //  Get extension.
            $extension = end($fileName);

            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            $targetFile = $targetPath . $name;

            move_uploaded_file($tempFile, $targetFile);

            // Generate response.
            $response = new \StdClass;
            $response->link = base_url() . $targetFile;

            // Send response.
            echo stripslashes(json_encode($response));
        } catch (Exception $e) {
            // Send error response.
            echo $e->getMessage();
            http_response_code(404);
        }
    }

    public function froala_manager()
    {
        try {
            $response = FroalaEditor_Image::getList('/smknusa-gitlab/uploads/img/post/');
            echo stripslashes(json_encode($response));
        } catch (Exception $e) {
            http_response_code(404);
        }
    }

    public function froala_delete()
    {
        $filePath = $_SERVER['HTTP_HOST'] . '/smknusa-gitlab/uploads/img/post/' . $_POST['name'];

        // Check if file exists.
        if (file_exists($filePath)) {
            // Delete file.
            return unlink($filePath);
        }
    }

    public function froala_file()
    {
        try {
            $tempFile = $_FILES['file']['tmp_name'];
            $fileName = explode(".", $_FILES['file']['name']);
            $targetPath = 'uploads/file/';

            //  Get extension.
            $extension = end($fileName);

            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            $targetFile = $targetPath . $name;

            move_uploaded_file($tempFile, $targetFile);

            // Generate response.
            $response = new \StdClass;
            $response->link = base_url() . $targetFile;

            // Send response.
            echo stripslashes(json_encode($response));
        } catch (Exception $e) {
            // Send error response.
            echo $e->getMessage();
            http_response_code(404);
        }
    }

    public function froala_video()
    {
        try {
            // File Route.
            $fileRoute = "uploads/file/";

            $fieldname = "file";

            // Get filename.
            $filename = explode(".", $_FILES[$fieldname]["name"]);

            // Get temp file name.
            $tmpName = $_FILES[$fieldname]["tmp_name"];

            // Get extension.
            $extension = end($filename);

            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;
            $fullNamePath = base_url() . $fileRoute . $name;

            // Save file in the uploads folder.
            move_uploaded_file($tmpName, $fullNamePath);

            // Generate response.
            $response = new \StdClass;
            $response->link = base_url() . $fileRoute . $name;

            // Send response.
            echo stripslashes(json_encode($response));

        } catch (Exception $e) {
            // Send error response.
            echo $e->getMessage();
            http_response_code(404);
        }
    }
}
