<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Profile_model', 'profile');
    }

//	VIEW
    public function index()
    {
        // setup page header data
        $this
            ->set_title("Profil");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Profile'
        );

        // load views
        $data['content'] = $this->load->view('admin/profile', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function update_users()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('name', 'Name', 'trim|required',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('failed', validation_errors());
            redirect(base_url('admin/profile'));
        } else {
            $config['encrypt_name'] = true;
            $config['upload_path'] = './uploads/img/photo_profile/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '10048';
            $config['remove_space'] = true;

            $this->load->library('upload');
            $this->upload->initialize($config);

            $id = $this->user['id'];

            if (!$this->upload->do_upload("photo")) {

                $data = array(
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'name' => $this->input->post('name'),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'updated_by' => $id,
                );

                $this->db->where('id', $id);
                $update = $this->db->update('users', $data);

                if ($update) {
                    $this->session->set_flashdata('success', '<div class="alert alert-success">Update Berhasil! Silahkan Logout dan Login kembali untuk mengganti sesi.</div>');
                    redirect(base_url('admin/profile'));
                } else {
                    $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Update Gagal!</div>');
                    redirect(base_url('admin/profile'));
                }

            } else {
                // GET IMAGE DETAIILS
                $file = array('upload_data' => $this->upload->data());

                // GET FILENAME WITHOUT EXTENSIONS
                $filename = pathinfo($file['upload_data']['file_name'], PATHINFO_FILENAME);
                // CALL IMAGE COMPRESS LIBRARY
                $compress = new Compress();
                // CONFIG COMPRESS
                $compress->file_url = base_url() . 'uploads/img/photo_profile/' . $file['upload_data']['file_name'];
                $compress->new_name_image = $filename . '_resz';
                $compress->quality = 50;
                $compress->destination = base_url() . 'uploads/img/photo_profile/';

                $data = array(
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'name' => $this->input->post('name'),
                    'photo' => $compress->compress_image()['image'],
                    'updated_at' => date("Y-m-d H:i:s"),
                    'updated_by' => $id,
                );

                $this->db->where('id', $id);
                $update = $this->db->update('users', $data);
                if ($update) {
                    $this->session->set_flashdata('success', '<div class="alert alert-success notif">Update Berhasil! Silahkan Logout dan Login kembali untuk mengganti sesi.</div>');
                    redirect(base_url('admin/profile'));
                } else {
                    $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Update Gagal!</div>');
                    redirect(base_url('admin/profile'));
                }
            }
        }
    }

    public function update_password()
    {
        $this->form_validation->set_rules('current_password', 'Password now', 'required|trim',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('new_password', 'New Password', 'required|trim',
            array('required' => 'Anda harus mengisi %s.'));
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|trim|matches[new_password]',
            array('required' => 'Anda harus mengisi %s.', 'matches' => 'Password yang anda masukan tidak sama.'));
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger notif">', '</div>');

        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('failed', validation_errors());
            redirect(base_url('admin/profile'));
        } else {
            $id = $this->user['id'];

            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password');

            $data = $this->profile->check_id($id);
            if ($data) {
                $valid_password = password_verify($current_password, $data->password);
                if ($valid_password) {

                    $data = array(
                        'password' => password_hash($new_password, PASSWORD_DEFAULT),
                    );

                    $this->db->where('id', $id);
                    $query = $this->db->update('users', $data);

                    if ($query) {
                        $this->session->set_flashdata('success', '<div class="alert alert-success notif">Update Berhasil!</div>');
                        redirect(base_url('admin/profile'));
                    } else {
                        $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Update Gagal!</div>');
                        redirect(base_url('admin/profile'));
                    }
                } else {
                    $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Password yang anda masukan salah.</div>');
                    redirect(base_url('admin/profile'));
                }
            }
        }
    }
}

