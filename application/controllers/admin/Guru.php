<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 7/26/2018
 * Time: 9:53 AM
 */

class Guru extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('guru_model', 'guru');

    }

    // VIEW UNTUK GALERI
    public function index()
    {
        // setup page header data
        $this
            ->set_title("Daftar Guru & Karyawan");

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Guru & Karyawan / Daftar Guru & Karyawan',
            'guru' => $this->guru->get()
        );

        // load views
        $data['content'] = $this->load->view('admin/guru/guru_daftar', $content_data, true);

        $this->load->view($this->template, $data);
    }

    public function add()
    {
        // setup page header data
        $this
            ->set_title("Tambah Guru")
            ->add_js_theme('guru_dropzone.js', TRUE);

        $data = $this->includes;

        $content_data = array(
            'sub' => 'Guru / Tambah Guru atau Karyawan',
        );

        // load views
        $data['content'] = $this->load->view('admin/guru/guru_tambah', $content_data, true);

        $this->load->view($this->template, $data);
    }

    // FUNCTION UNTUK GALERI
    public function get_where($id)
    {
        header('Content-Type: application/json');
        $data = array();

        $query = $this->guru->get($id);

        foreach ($query as $key => $value) {
            $data = array(
                'id' => $value->id,
                'image' => $value->image,
                'name' => $value->name,
                'profession' => $value->profession,
                'year' => $value->year,
                'email' => $value->email,
                'date' => date('d F Y', strtotime($value->created_at)),
            );
        }

        echo json_encode($data);
    }

    public function add_photo()
    {
        $data = array();

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $fileName = explode(".", $_FILES['file']['name']);
            //  Get extension.
            $extension = end($fileName);
            // Generate new random name.
            $name = sha1(microtime()) . "." . $extension;

            $targetPath = './uploads/img/guru/';

            $targetFile = $targetPath . $name;

            move_uploaded_file($tempFile, $targetFile);

            // GET FILENAME WITHOUT EXTENSIONS
            $filenameWithoutExt = pathinfo($name, PATHINFO_FILENAME);
            // CALL IMAGE COMPRESS LIBRARY
            $compress = new Compress();
            // CONFIG COMPRESS
            $compress->file_url = base_url() . 'uploads/img/guru/' . $name;
            $compress->new_name_image = $filenameWithoutExt . '_resz';
            $compress->quality = 50;
            $compress->destination = base_url() . 'uploads/img/guru/';

            $data = array(
                'name' => $this->input->post('teacher_name'),
                'profession' => $this->input->post('teacher_profession'),
                'year' => $this->input->post('teacher_year'),
                'email' => $this->input->post('teacher_email'),
                'image' => $compress->compress_image()['image'],
                'created_by' => $this->user['id'],
                'created_at' => date("Y-m-d H:i:s"),
                'is_deleted' => 0
            );

            $query = $this->db->insert('teacher', $data);

            if ($query) {
                $data = array(
                    'status' => 200
                );
            } else {
                $data = array(
                    'status' => 400
                );
            }
        }
        echo json_encode($data);
    }

    public function update_post($id)
    {
        $config['encrypt_name'] = true;
        $config['upload_path'] = './uploads/img/guru/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '10480';
        $config['remove_space'] = true;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if (!$this->upload->do_upload("photo_teacher")) {

            $data = array(
                'name' => $this->input->post('teacher_name'),
                'profession' => $this->input->post('teacher_profession'),
                'year' => $this->input->post('teacher_year'),
                'email' => $this->input->post('teacher_email'),
                'updated_by' => $this->user['id'],
                'updated_at' => date("Y-m-d H:i:s"),
                'is_deleted' => 0
            );

            $this->db->where('teacher.id', $id);
            $query = $this->db->update('teacher', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Diperbarui!</div>');
                redirect(base_url('admin/guru/'));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
                redirect(base_url('admin/guru/' . $id));
            }
        } else {
            $file = array('upload_data' => $this->upload->data());

            $data = array(
                'image' => $file['upload_data']['file_name'],
                'name' => $this->input->post('teacher_name'),
                'profession' => $this->input->post('teacher_profession'),
                'year' => $this->input->post('teacher_year'),
                'email' => $this->input->post('teacher_email'),
                'updated_by' => $this->user['id'],
                'updated_at' => date("Y-m-d H:i:s"),
                'is_deleted' => 0
            );

            $this->db->where('teacher.id', $id);
            $query = $this->db->update('teacher', $data);

            if ($query) {
                $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Diperbarui!</div>');
                redirect(base_url('admin/guru/'));
            } else {
                $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Diperbarui!</div>');
                redirect(base_url('admin/guru/' . $id));
            }
        }
    }

    public function delete_post($id)
    {
        $data = array(
            'is_deleted' => 1,
            'deleted_at' => date("Y-m-d H:i:s"),
            'deleted_by' => $this->user['id']
        );
        $this->db->where('teacher.id', $id);
        $query = $this->db->update('teacher', $data);

        if ($query) {
            $this->session->set_flashdata('success', '<div class="alert alert-success notif">Photo Berhasil Dihapus!</div>');
            redirect(base_url('admin/guru'));
        } else {
            $this->session->set_flashdata('failed', '<div class="alert alert-danger notif">Photo Gagal Dihapus!</div>');
            redirect(base_url('admin/guru'));
        }
    }
}