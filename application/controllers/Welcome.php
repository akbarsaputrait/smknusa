<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Public_Controller
{

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // load the language file
        $this->lang->load('welcome');
        $this->load->model('guru_model', 'guru');
        $this->load->model('post_model', 'post');
        $this->load->model('galeri_model', 'galeri');
        $this->load->model('fasilitas_model', 'fasilitas');

    }


    /**
     * Default
     */
    function index()
    {
        // setup page header data
        $this->set_title(sprintf(lang('welcome title'), $this->settings->site_name))
            ->add_js_theme(array(
                'script.js',
                'slick/slick.min.js',
            ))
            ->add_css_theme(array(
                'slick/slick.css',
                'slick/slick-theme.css',
                'css.css',
                'blog/frontend.css'
            ));

        $data = $this->includes;

        // set content data
        $content_data = array(
            'guru' => $this->guru->get(),
            'post' => $this->post->get('blog', 6, null, true, false),
            'galeri' => $this->galeri->get(null, 12),
            'fasilitas' => $this->fasilitas->get(null, null),
        );

        // load views
        $data['content'] = $this->load->view('home', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function list_blog()
    {
        // setup page header data
        $this->set_title("Daftar Blog")
            ->add_css_theme('blog/frontend.css');

        $data = $this->includes;

        // set content data
        $content_data = array(
            // 'welcome_message' => $this->settings->welcome_message[$this->session->language]
            'title' => "Daftar Blog"
        );

        // load views
        $data['content'] = $this->load->view('list_blog', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function blog($slug)
    {
        // setup page header data
        $this->set_title("Judul Blog")
            ->add_css_theme('blog/frontend.css');

        $data = $this->includes;

        $array_slug = explode('-', $slug);
        $id = $array_slug[0];

        // set content data
        $content_data = array(
            // 'welcome_message' => $this->settings->welcome_message[$this->session->language]
            'title' => "Judul Blog",
            'post' => $this->post->get_where('blog', $id)
        );

        // load views
        $data['content'] = $this->load->view('blog', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function list_news()
    {
        // setup page header data
        $this->set_title("Daftar Pengumuman")
            ->add_css_theme('blog/frontend.css');

        $data = $this->includes;

        // set content data
        $content_data = array(
            // 'welcome_message' => $this->settings->welcome_message[$this->session->language]
            'title' => "Daftar Pengumuman"
        );

        // load views
        $data['content'] = $this->load->view('list_news', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }

    function news()
    {
        // setup page header data
        $this->set_title("Judul Pengumuman")
            ->add_css_theme('blog/frontend.css');

        $data = $this->includes;

        // set content data
        $content_data = array(
            // 'welcome_message' => $this->settings->welcome_message[$this->session->language]
            'title' => "Judul Pengumuman"
        );

        // load views
        $data['content'] = $this->load->view('news', $content_data, TRUE);
        $this->load->view($this->template, $data);
    }
}
