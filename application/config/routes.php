<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// FRONTEND
$route['default_controller']                                    = 'home';
// POST
$route['post/(:any)/(:any)']                                    = 'frontend/post/index/$1/$2';
$route['posts/list/(:any)']                                     = 'frontend/post/list/$1';
$route['posts/list/(:any)/(:any)']                              = 'frontend/post/list/$1/$2';
// EVENT
$route['event']                                                 = 'frontend/agenda/index';
$route['event/(:any)']                                          = 'frontend/agenda/$1';
// TEACHER
$route['teachers']                                              = 'frontend/guru/index';
$route['teachers/(:any)']                                       = 'frontend/guru/index/$1';
// JURUSA
$route['jurusan/(:any)/(:any)']                                 = 'frontend/jurusan/index/$1/$2';
// GALERI
$route['galeri']                                                = 'frontend/galeri/index';
$route['galeri/(:any)']                                         = 'frontend/galeri/index/$1';

// FASILITAS
$route['fasilitas']                                             = 'frontend/fasilitas/index';
$route['fasilitas/(:any)']                                      = 'frontend/fasilitas/index/$1';

// AUTH
$route['admin/login']                                           = 'user/login';
$route['logout']                                                = 'user/logout';

// PROFILE
$route['profile']                                               = 'frontend/profile/index';
$route['profile/(:any)']                                        = 'frontend/profile/$1';
$route['profile/(:any)/(:any)']                                 = 'frontend/profile/$1/$2';


// ADMIN
$route['admin']                                                 = 'admin/dashboard';
$route['admin/profile']                                         = 'admin/profile';
$route['admin/blog/(:any)/(:any)']                              = 'admin/blog/$1/$2';
$route['admin/pengumuman/(:any)/(:any)']                        = 'admin/pengumuman/$1/$2';
$route['admin/galeri/(:any)/(:any)']                            = 'admin/galeri/$1/$2';
$route['admin/fasilitas/(:any)/(:any)']                         = 'admin/fasilitas/$1/$2';
$route['admin/agenda/(:any)/(:any)']                            = 'admin/agenda/$1/$2';
$route['admin/guru/(:any)/(:any)']                              = 'admin/guru/$1/$2';
$route['admin/program_keahlian/(:any)/(:any)/(:any)/(:any)']    = 'admin/program_keahlian/$1/$2/$3/$4';
$route['admin/settings']                                        = 'admin/settings';
$route['admin/link']                                            = 'admin/link/index';
$route['admin/link/(:any)']                                     = 'admin/link/$1';
$route['admin/alumni']                                          = 'admin/alumni/index';
$route['admin/alumni/(:any)']                                   = 'admin/alumni/$1';

$route['sitemap\.xml']                                          = 'sitemap';

$route['404_override']                                          = 'errors/error404';
$route['translate_uri_dashes']                                  = TRUE;