<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/17/2018
 * Time: 7:18 PM
 */

class Alumni_Model extends CI_Model
{
    protected $table = 'alumni';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($status = NULL, $id = NULL, $order = NULL, $created_by = NULL)
    {
        $result = NULL;

        $sql = "SELECT * FROM {$this->table} WHERE is_deleted = 0 ";

        if (!is_null($status)) {
            $sql .= " AND status = '{$status}'";
        }

        if (!is_null($id)) {
            $sql .= " AND id = {$id}";
        }

        if (!is_null($created_by)) {
            $sql .= " AND created_by IS NULL";
        }

        if (!is_null($order)) {
            $sql .= " ORDER BY name ASC";
        } else {
            $sql .= " ORDER BY id DESC";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
}