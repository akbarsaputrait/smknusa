<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    private $_post_db;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_post_db = 'post';
    }

    public function count_post()
    {
        return $this->db->count_all($this->_post_db);
    }

    public function get_post($type = NULL, $limit = 5)
    {
        $results = NULL;

        $sql = "SELECT * FROM {$this->_post_db} WHERE is_deleted = 0";
        if (!is_null($type)) {
            $sql .= " AND type = '{$type}'";
        }

        $sql .= " ORDER BY created_at DESC LIMIT {$limit}";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
            $results = $query->result_array();
        }

        return $results;
    }
}
