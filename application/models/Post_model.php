<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post_model extends CI_Model
{
    private $_post_db;
    private $_user_db;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_post_db = 'post';
        $this->_user_db = 'users';
    }

    public function get($type = NULL, $limit = NULL, $offset = NULL, $publish = true, $random = false, $will_show = false)
    {
        $results = NULL;
        $sql = "SELECT post.id, post.title, post.type, post.image, post.content, post.status, post.slug, DATE(post.created_at) AS created_at, DATE(post.start_date) AS start_date, DATE(post.end_date) AS end_date, users.name FROM post JOIN users ON post.created_by = users.id WHERE post.is_deleted = 0";

        if ($publish) {
            $sql .= " AND post.status = 'publish' ";
        }

        if (!is_null($type)) {
            $sql .= " AND post.type = '{$type}'";
        }

        if ($will_show == TRUE) {
            $sql .= " AND (NOW() BETWEEN post.start_date AND post.end_date)";
        }

        if ($random == TRUE) {
            $sql .= " ORDER BY RAND() ";
        } else {
            $sql .= " ORDER BY post.id DESC";
        }

        if (!is_null($limit)) {
            $sql .= " LIMIT {$limit}";
        }

        if (!is_null($offset)) {
            $sql .= " OFFSET {$offset}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $results = $query->result();
        }

        return $results;
    }

    public function get_where($type = null, $id = null)
    {
        $result = NULL;

        $sql = "SELECT post.id, post.type, post.title, post.status, post.slug, post.content, post.image, post.categories, post.tags, DATE(post.created_at) AS created_at, DATE(post.start_date) AS start_date, DATE(post.end_date) AS end_date, users.name FROM post JOIN users ON post.created_by = users.id WHERE post.is_deleted = 0";

        if (!is_null($id)) {
            $sql .= " AND post.id = '{$id}'";
        }

        if (!is_null($type)) {
            $sql .= " AND post.type = '{$type}'";
        }

        $sql .= " LIMIT 1";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }

    public function count_all($type)
    {
        $result = NULL;

        $sql = "SELECT post.id, post.title, post.type, post.image, post.content, post.status, post.slug, DATE(post.created_at) AS created_at, users.name FROM post JOIN users ON post.created_by = users.id WHERE post.is_deleted = 0";

        $sql .= " AND post.type = '{$type}'";

        $query = $this->db->query($sql);

        $result = $query->num_rows();

        return $result;
    }
}
