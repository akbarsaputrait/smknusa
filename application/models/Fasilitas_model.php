<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fasilitas_model extends CI_Model
{
    private $_fasilitas_db;
    private $_user_db;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_fasilitas_db = 'facility';
        $this->_user_db = 'users';
    }

    public function get($id = NULL, $limit = NULL, $offset = NULL)
    {
        $results = NULL;

        $sql = "SELECT facility.id, facility.image, facility.name AS nama_fasilitas, facility.content, DATE(facility.created_at) AS created_at, users.name FROM facility JOIN users ON facility.created_by = users.id WHERE facility.is_deleted = 0  ";

        if (!is_null($id)) {
            $sql .= " AND facility.id = {$id}";
        }

        $sql .= " ORDER BY facility.id DESC";

        if (!is_null($limit)) {
            $sql .= " LIMIt {$limit}";
        }

        if (!is_null($offset)) {
            $sql .= " OFFSET {$offset}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $results = $query->result();
        }

        return $results;
    }

    public function count_all()
    {
        $result = NULL;

        $sql = "SELECT facility.id, facility.image, facility.name AS nama_fasilitas, facility.content, DATE(facility.created_at) AS created_at, users.name FROM facility JOIN users ON facility.created_by = users.id WHERE facility.is_deleted = 0";

        $query = $this->db->query($sql);

        $result = $query->num_rows();

        return $result;
    }
}
