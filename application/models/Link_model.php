<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 8/16/2018
 * Time: 9:28 AM
 */

class Link_model extends CI_Model
{
    protected $table = 'link';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($id = NULL)
    {
        $result = NULL;

        $sql = "SELECT * FROM {$this->table} WHERE is_deleted = 0 ";

        if (!is_null($id)){
            $sql .= " AND id = {$id}";
        }

        $sql .= " ORDER BY id DESC";

        $query = $this->db->query($sql);

        if($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
}