<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keahlian_model extends CI_Model
{
    public $table = 'class';
    public $join = 'class_image';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($type = NULL, $id = NULL, $order = NULL)
    {
        $result = NULL;

        $sql = "SELECT class.id, class.name, class.type, class.since, class.content, DATE(class.created_at) AS created_at, class_image.image FROM {$this->table} JOIN {$this->join} ON class.id = class_image.class_id WHERE class.is_deleted = 0 AND class_image.is_deleted = 0";

        if (!is_null($type)) {
            $sql .= " AND class.type = '{$type}'";
        }

        if (!is_null($id)) {
            $sql .= " AND class.id = {$id}";
        }

        $sql .= " GROUP BY class.name";

        if (!is_null($order)) {
            $sql .= " ORDER BY class.type DESC";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }

    public function get_image($class_id)
    {
        $result = NULL;

        $sql = "SELECT id, image, DATE(created_at) AS created_at FROM {$this->join} WHERE is_deleted = 0 AND class_id = {$class_id}";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }

    public function get_image_where($id)
    {
        $result = NULL;

        $sql = "SELECT id, image, DATE(created_at) AS created_at FROM {$this->join} WHERE is_deleted = 0 AND id = {$id}";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
}
