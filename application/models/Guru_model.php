<?php
/**
 * Created by PhpStorm.
 * User: YUDHA
 * Date: 7/26/2018
 * Time: 10:17 AM
 */

class Guru_model extends CI_Model
{
    private $_guru_db;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_guru_db = 'teacher';
    }


    public function get($id = NULL, $limit = NULL, $offset = NULL, $random = NULL)
    {
        $results = NULL;

        $sql = "SELECT * FROM " . $this->_guru_db . " WHERE is_deleted = 0";

        if(!is_null($id)) {
            $sql .= " AND id = {$id}";
        }

        if(!is_null($random)) {
            $sql .= " ORDER BY RAND() ";
        } else {
            $sql .= " ORDER BY id DESC";
        }

        if(!is_null($limit)) {
            $sql .= " LIMIT {$limit}";
        }

        if(!is_null($offset)) {
            $sql .= " OFFSET {$offset}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $results = $query->result();
        }

        return $results;
    }

    public function count_all()
    {
        $result = NULL;

        $sql = "SELECT * FROM " . $this->_guru_db . " WHERE teacher.is_deleted = 0  ";
        $sql .= " ORDER BY teacher.id DESC";

        $query = $this->db->query($sql);

        $result = $query->num_rows();

        return $result;
    }
}