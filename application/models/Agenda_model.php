<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agenda_model extends CI_Model
{
    public $table = 'event';
    public $join = 'users';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_agenda($limit = NULL)
    {
        $result = NULL;

        $sql = "SELECT id, title, start_date, end_date, background_color, text_color, url FROM event WHERE is_deleted = 0 ORDER BY id DESC";

        if (!is_null($limit)){
            $sql .= " LIMIT {$limit}";
        }

        $query = $this->db->query($sql);

        if($query->num_rows() > 0) {
            $result = $query->result();
        }

        return $result;
    }
}
