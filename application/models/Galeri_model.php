<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Galeri_model extends CI_Model
{
    private $_galeri_db;
    private $_user_db;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // define primary table
        $this->_galery_db = 'galery';
        $this->_user_db = 'users';
    }

    public function get($id = NULL, $limit = NULL, $offset = NULL)
    {
        $results = NULL;

        $sql = "SELECT galery.id, galery.filename, galery.categories, galery.content, DATE(galery.created_at) AS created_at, users.name FROM galery JOIN users ON galery.created_by = users.id WHERE galery.is_deleted = 0  ";

        if (!is_null($id)) {
            $sql .= " AND galery.id = {$id}";
        }

        $sql .= " ORDER BY galery.id DESC";

        if (!is_null($limit)) {
            $sql .= " LIMIt {$limit}";
        }

        if (!is_null($offset)) {
            $sql .= " OFFSET {$offset}";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $results = $query->result();
        }

        return $results;
    }

    public function count_all()
    {
        $result = NULL;

        $sql = "SELECT galery.id, galery.filename, galery.categories, galery.content, DATE(galery.created_at) AS created_at, users.name FROM galery JOIN users ON galery.created_by = users.id WHERE galery.is_deleted = 0  ";

        $query = $this->db->query($sql);

        $result = $query->num_rows();

        return $result;
    }
}
