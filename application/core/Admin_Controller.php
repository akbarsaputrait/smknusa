<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Base Admin Class - used for all administration pages
 */
class Admin_Controller extends MY_Controller
{

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        // must be logged in
        if (!$this->user) {
            if (current_url() != base_url()) {
                //store requested URL to session - will load once logged in
                $data = array('redirect' => current_url());
                $this->session->set_userdata($data);
            }

            redirect('admin/login');
        }

        // make sure this user is setup as admin
        if (!$this->user['is_active']) {
            redirect(base_url());
        }

        // load the admin language file
        $this->lang->load('admin');

        // prepare theme name
        $this->settings->theme = strtolower($this->config->item('admin_theme'));

        // set up global header data
        $this
            ->add_css_theme(array(
                "{$this->settings->theme}.css",
                "summernote-bs3.css",
                "simple-line-icons/css/simple-line-icons.css",
                "styles.css",
                "my.css",
                "plugin/datatables/responsive.bootstrap4.min.css",
                "plugin/datatables/dataTables.bootstrap4.min.css",
                "plugin/datatables/fixedHeader.bootstrap4.min.css",
                "plugin/datatables/buttons.bootstrap4.min.css",
                "plugin/sweetalert2/sweetalert2.min.css",
                "plugin/dropzone/dropzone.css",
                "plugin/froala/css/froala_editor.pkgd.min.css",
                "plugin/froala/css/froala_style.min.css",
                "plugin/froala/css/froala_editor.min.css",
                "plugin/froala/css/codemirror.min.css",
                "plugin/froala/css/plugins/colors.min.css",
                "plugin/froala/css/plugins/file.min.css",
                "plugin/froala/css/plugins/emoticons.min.css",
                "plugin/froala/css/plugins/image.min.css",
                "plugin/froala/css/plugins/image_manager.min.css",
                "plugin/froala/css/plugins/table.min.css",
                "plugin/tag-input/jquery.tagsinput.min.css",
                "plugin/jquery-ui/jquery-ui.min.css",
                "plugin/jquery-ui/jquery-ui-timepicker-addon.css",
                "plugin/fullcalendar/fullcalendar.css"
            ))
            ->add_js_theme(array(
                "summernote.min.js",
                "carbon.js",
                "demo.js",
                "admin.js",
                "plugin/datatables/jquery.dataTables.min.js",
                "plugin/datatables/dataTables.responsive.min.js",
                "plugin/datatables/responsive.bootstrap4.js",
                "plugin/datatables/dataTables.bootstrap4.min.js",
                "plugin/datatables/dataTables.fixedHeader.min.js",
                "plugin/sweetalert2/sweetalert2.all.min.js",
                "plugin/dropzone/dropzone.js",
                "plugin/froala/js/froala_editor.min.js",
                "plugin/froala/js/froala_editor.pkgd.min.js",
                "plugin/froala/js/codemirror.min.js",
                "plugin/froala/js/xml.min.js",
                "plugin/froala/js/plugins/align.min.js",
                "plugin/froala/js/plugins/colors.min.js",
                "plugin/froala/js/plugins/file.min.js",
                "plugin/froala/js/plugins/font_family.min.js",
                "plugin/froala/js/plugins/font_size.min.js",
                "plugin/froala/js/plugins/image.min.js",
                "plugin/froala/js/plugins/image_manager.min.js",
                "plugin/froala/js/plugins/lists.min.js",
                "plugin/froala/js/plugins/table.min.js",
                "plugin/froala/js/plugins/word_paste.min.js",
                "plugin/tag-input/jquery.tagsinput.min.js",
                "plugin/jquery-ui/jquery-ui.min.js",
                "plugin/jquery-ui/jquery-ui-timepicker-addon.js",
                "plugin/fullcalendar/moment.min.js",
                "plugin/fullcalendar/fullcalendar.js",
                "plugin/fullcalendar/locale/id.js"))
            ->add_js_theme("{$this->settings->theme}_i18n.js", true);

        // declare main template
        $this->template = "../../{$this->settings->themes_folder}/{$this->settings->theme}/templates.php";
    }

}
