<!-- Carousel -->
<div class="slider single-item" style="padding-top: 4em!important;">
    <?php foreach ($posts as $item): ?>
        <div class="row">
            <div class="CoverImage FlexEmbed FlexEmbed--0by1 lazy"
                 data-src="<?= base_url() . 'uploads/img/cover_post/' . $item->image ?>">
                <div class="captions">
                    <h3 class="badge badge-dark text-white">
                        <?= date('d F Y', strtotime($item->created_at));?>
                    </h3>
                    <h2>
                        <a href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>"
                           class="badge badge-light text-dark text-left" style="white-space: inherit;">
                            <?= $item->title; ?>
                        </a>
                    </h2>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<section class="bg-white text-center" style="padding: 30px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header bg-warning text-white">
                        <b><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Pengumuman Terbaru</b>
                    </div>
                    <?php
                    if (is_null($pengumuman)) {
                        echo '<div class="alert alert-danger m-0">Tidak ada pengumuman terbaru.</div>';
                    } else {
                        ?>
                        <div class="animationLoading">
                            <?php
                            foreach ($pengumuman as $item):
                                ?>
                                <div class="list-group p-0 animatedNews">
                                    <a href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>"
                                       class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h6 class="mb-1">
                                                <strong>
                                                    <?= $item->title; ?>
                                                </strong>
                                            </h6>
                                            <small>
                                                <?= date('d F Y', strtotime($item->created_at)); ?>
                                            </small>
                                        </div>
                                        <small>
                                            <?php
                                            $string = strip_tags($item->content);
                                            if (strlen($string) > 200) {

                                                // truncate string
                                                $stringCut = substr($string, 0, 100);
                                                $endPoint = strrpos($stringCut, ' ');

                                                //if the string doesn't contain any space then it will cut without word basis.
                                                $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                $string .= '...';
                                            }
                                            echo $string;
                                            ?>
                                        </small>
                                    </a>
                                </div>
                            <?php
                            endforeach;
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="card-footer text-dark text-right">
                        <b><a href="<?= base_url() . 'posts/list/pengumuman' ?>">Lihat Lainnya <i
                                        class="fa fa-arrow-right"></i></a></b>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header bg-primary text-white">
                        <b><i class="fa fa-calendar" aria-hidden="true"></i> Agenda Terbaru</b>
                    </div>

                    <?php
                    foreach ($agenda as $item):
                        ?>
                        <div class="list-group p-0">
                            <a href="<?= base_url() . 'event'; ?>"
                               class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <h6 class="mb-1 text-left">
                                        <strong>
                                            <?= $item->title; ?>
                                        </strong>
                                    </h6>
                                    <small>
                                        <?= date('d F Y', strtotime($item->start_date)); ?>
                                    </small>
                                </div>
                            </a>
                        </div>
                    <?php
                    endforeach;
                    ?>

                    <div class="card-footer text-dark text-right">
                        <b><a href="<?= base_url() . 'event' ?>">Lihat Lainnya <i
                                        class="fa fa-arrow-right"></i></a></b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="text-center" style="background: #f0f0f0; padding: 37px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mt-4">
                <div class="d-flex justify-content-center">
                    <h1>
                        <span class="badge badge-danger">Jurusan</span>
                    </h1>
                </div>
                <div class="d-flex align-items-center">
                    <p>SMK Negeri 1 Purwosari memiliki 4 program keahlian yang mana dibagi menjadi 10 macam jurusan.</p>
                </div>
            </div>
            <?php
            foreach ($jurusan as $item):
                ?>
                <div class="col-md-2 mt-4">
                    <div class="img-jurusan">
                        <div class="CoverImage FlexEmbed FlexEmbed--100 lazy"
                             data-src="<?= base_url() . 'uploads/img/jurusan/' . $item->image; ?>"></div>
                        <a href="<?= base_url() . 'jurusan/' . $item->type . '/' . $item->id ?>" class="text-white"
                           style=" text-decoration: none;">
                            <div class="ovrly"></div>
                            <p><?= $item->name; ?></p>
                        </a>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
</section>

<section class="bg-white text-center" style="padding: 50px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="slider single-item">
                    <?php foreach ($fasilitas as $item): ?>
                        <div>
                            <div class="CoverImage FlexEmbed FlexEmbed--16by9 lazy"
                                 data-src="<?= base_url() . 'uploads/img/fasilitas/' . $item->image ?>"></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-md-6 text-left pt-3">
                <div class="w-100 d-flex justify-content-start">
                    <h1>
                        <span class="badge badge-info">Fasilitas</span>
                    </h1>
                </div>
                <p>Fasilitas yang baik dapat menunjang proses pembelajaran dan peningkatan karakter warga sekolah.</p>
                <div class="d-flex justify-content-start align-item-center mb-3">
                    <a href="<?= base_url('fasilitas') ?>" style="" class="btn btn-raised btn-dark text-white">
                        <i>Lihat selengkapnya</i> &nbsp;
                        <i class="fa fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content-pd-100 bg-dark text-white">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-5">
                <div class="w-100 d-flex justify-content-start">
                    <h1>
                        <span class="badge badge-primary">Guru</span>
                    </h1>
                </div>
                <p>Guru adalah sosok manusia dengan kelembutan hati dan kasih sayangnya yang tak ada duanya. Maka dari
                    itu, sayangi dan hormati guru, karena tanpa ilmu yang diberi olehnya kita tak akan jadi apa - apa.
                </p>
                <div class="d-flex justify-content-start align-item-center mb-3">
                    <a href="<?= base_url('teachers') ?>" style="" class="btn btn-raised btn-light text-dark">
                        <i>Lihat selengkapnya</i> &nbsp;
                        <i class="fa fa-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-7">
                <div class="row poto ">
                    <?php foreach ($guru as $item): ?>
                        <div class="col-md-4 hover-img teacher-1 pb-4">
                            <div class="card">
                                <div class="CoverImage FlexEmbed FlexEmbed--100 lazy"
                                     data-src="<?= base_url() . 'uploads/img/guru/' . $item->image ?>"></div>
                                <div class="overlay">
                                    <div class="text">
                                        <h5>
                                            <b><?= $item->name; ?></b>
                                        </h5>
                                        <p><?= $item->profession; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content-pd-100">
    <div class="container blog text-center">
        <div class="w-100 d-flex justify-content-center">
            <h1>
                <span class="badge badge-success">Blog</span>
            </h1>
        </div>
        <p>Berikut adalah beberapa kegiatan dari
            <br>SMK Negeri 1 Purwosari.</p>
        <div class="row">
            <div class="col-md-12 d-flex justify-content-start align-item-center mb-3">
                <a href="<?= base_url('posts/list/blog') ?>" class="btn btn-raised btn-dark text-white">
                    <i>Lihat Lainnya</i> &nbsp;
                    <i class="fa fa-arrow-right"></i>
                </a>
            </div>
        </div>
        <div class="row" style="">
            <?php foreach ($post as $item): ?>
                <div class="col-md-4 pb-3 text-left">
                    <div class="containera card">
                        <div class="CoverImage FlexEmbed FlexEmbed--2by1 lazy"
                             data-src="<?= base_url() . 'uploads/img/cover_post/' . $item->image ?>"></div>
                        <div class="card-body" style="padding:20px 30px;">
                            <h6 class="">
                                <b><?= $item->title; ?></b>
                            </h6>
                            <div class="">
                                    <?php
                                        $string = strip_tags($item->content);
                                        if (strlen($string) > 200) {

                                            // truncate string
                                            $stringCut = substr($string, 0, 100);
                                            $endPoint = strrpos($stringCut, ' ');

                                            //if the string doesn't contain any space then it will cut without word basis.
                                            $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                            $string .= '...';
                                        }
                                        echo $string;
                                        ?>
                            </div>
                            <div class="d-flex justify-content-end">
                                <i><?= $item->name; ?></i>
                            </div>
                        </div>
                        <div class="overlay">
                            <div class="card-body">
                                <div class="mb-4"><i><?= date('d F Y', strtotime($item->created_at)); ?></i></div>
                                <a class="text-dark"
                                   href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>">
                                    <h5><b><?= $item->title; ?></b></h5>
                                </a>
                                <?php
                                $string = strip_tags($item->content);
                                if (strlen($string) > 500) {

                                    // truncate string
                                    $stringCut = substr($string, 0, 200);
                                    $endPoint = strrpos($stringCut, ' ');

                                    //if the string doesn't contain any space then it will cut without word basis.
                                    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string .= '...';
                                }
                                echo $string;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>