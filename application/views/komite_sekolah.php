
    <div class="<?= $color;?>">
        <div class="container">
            <div class="text-center text-white mb-4">
                <h1><b><?= $title;?></b></h1>
            </div>
            <div class="card padding-content">
                <div class="CoverImage FlexEmbed FlexEmbed--2by1" style="background-image:url(<?= base_url() . 'assets/themes/default/img/photo.png'?>);"></div>
            </div>
            <div class="card">
                <div class="content-blog">
                    <p style="text-align:justify;"> &nbsp; &nbsp; &nbsp; Semenjak diluncurkannya Management Peningkatan Mutu Berbasis Sekolah dalam sistem
                        management sekolah, komite sekolah sebagai organisasi mitra sekolah memiliki peran yang sangat strategis
                        dalam upaya turut serta mengembangkan pendidikan disekolah. Kehadirannya tidak hanya sekedar sebagai
                        stempel sekolah semata, khususya dalam upaya memungut biaya dari orang tua siswa, namun lebih jauh
                        Komite Sekolah harus dapat menjadi sebuah organisasi yang bena-banar dapat mewadahi dan menyalurkan
                        aspirasi serta prakarsa dan masyarakat dalam melahirkan kebijakan operasional dan program pendidikan
                        disekolah serta dapat mencitakan suasana dan kondisi transparan, akuntabel, dan demokratis dalam
                        penyelenggaraan dan pelayanan pendidikan yang bermutu disekolah.</p>
                    <p style="text-align:justify;"> &nbsp; &nbsp; &nbsp; Agar Komite Sekolah dapat berdaya, maka dalam pembentukan penguruspun harus dapat
                        memenuhi beberapa prinsip / kaidah dan mekasime yang benar, serta dapat dikelola secara benar pula.</p>
                </div>
            </div>
        </div>

         <!-- start : content article -->
         <div class="container">
             <div class="pt-5">
                 <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
                 <div class="row" style="">
                     <?php foreach ($post as $item): ?>
                         <div class="col-md-4 pb-3 text-left">
                             <div class="containera card">
                                 <div class="CoverImage FlexEmbed FlexEmbed--2by1 lazy"
                                      data-src="<?= base_url() . 'uploads/img/cover_post/' . $item->image ?>"></div>
                                 <div class="card-body" style="padding:20px 30px;">
                                     <h6 class="">
                                         <b><?= $item->title; ?></b>
                                     </h6>
                                     <div class="">
                                    <?php
                                        $string = strip_tags($item->content);
                                        if (strlen($string) > 200) {

                                            // truncate string
                                            $stringCut = substr($string, 0, 100);
                                            $endPoint = strrpos($stringCut, ' ');

                                            //if the string doesn't contain any space then it will cut without word basis.
                                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                                            $string .= '...';
                                        }
                                        echo $string;
                                    ?>
                            </div>
                                     <div class="d-flex justify-content-end">
                                         <i><?= $item->name; ?></i>
                                     </div>
                                 </div>
                                 <div class="overlay">
                                     <div class="card-body">
                                         <div class="mb-4"><i><?= date('d F Y', strtotime($item->created_at)); ?></i></div>
                                         <a class="text-dark"
                                            href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>">
                                             <h5><b><?= $item->title; ?></b></h5>
                                         </a>
                                         <?php
                                         $string = strip_tags($item->content);
                                         if (strlen($string) > 500) {

                                             // truncate string
                                             $stringCut = substr($string, 0, 200);
                                             $endPoint = strrpos($stringCut, ' ');

                                             //if the string doesn't contain any space then it will cut without word basis.
                                             $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                             $string .= '...';
                                         }
                                         echo $string;
                                         ?>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     <?php endforeach; ?>
                 </div>
             </div>
        </div>
        <!-- end : content article -->
    </div>