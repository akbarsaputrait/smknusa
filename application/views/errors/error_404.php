<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-md-12 d-flex justify-content-center align-items-center text-center" style="height: 100vh;">
        <div>
            <h1>Maaf, konten yang anda maksud tidak ditemukan.</h1>
            <a href="<?= base_url();?>" class="btn btn-raised btn-dark"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
    </div>
</div>
