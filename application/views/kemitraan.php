<div class="<?=$color;?>">
    <div class="container">
        <div class="text-center text-white mb-4">
            <h1><b><?=$title;?></b></h1>
        </div>
        <div class="card padding-content">
            <div class="content-blog">
                <table class="table table-bordered" id="datatable">
                    <thead class="text-center" style="background-color:#00B469;">
                    <tr>
                        <th style="color:#f0f0f0;">Kategori</th>
                        <th style="color:#f0f0f0;">Nama Industri</th>
                        <th style="color:#f0f0f0;">Alamat Industri</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td rowspan="3">Lembaga Pemeritah</td>
                        <td>BPTP Jawa Timur</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>BPSP Jawa Timur</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Balijestro Malang</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td rowspan="27">Lembaga Swasta</td>
                        <td>Auto 2000</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Astra International</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. East West Seed Indonesia</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Garuda Food</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Pertrpkimia tbk.</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Panasonic Indonesia</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Karya Mitra</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Sarana Tani Indonesia</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Roda Mas</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Gutner Beji</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Nissan</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Toyota Boshoku Indonesia</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>BBI Pasuruan</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Japan Indonesia Economy Centre (JIAEC)</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. PINDAD</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. PAL Surabaya</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Cheil Jedang Indonesia (CJI)</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Panverta</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Supra Alumunium Indonesia</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Adiputro Wira Sejati</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Shang Hyang Seri</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Karya Dibya Mahardika</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Tirta Bahagia (Air Minum Club)</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Otsuka Indah Amerta (Pocari Sweet)</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>CV. Gunung Arta</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Houtech</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PT. Shelter Indonesia</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- start : content article -->
    <div class="container">
        <div class="pt-5">
            <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
            <div class="row" style="">
                <?php foreach ($post as $item): ?>
                    <div class="col-md-4 pb-3 text-left">
                        <div class="containera card">
                            <div class="CoverImage FlexEmbed FlexEmbed--2by1 lazy"
                                 data-src="<?=base_url() . 'uploads/img/cover_post/' . $item->image?>"></div>
                            <div class="card-body" style="padding:20px 30px;">
                                <h6 class="">
                                    <b><?=$item->title;?></b>
                                </h6>
                                <div class="">
                                    <?php
$string = strip_tags($item->content);
if (strlen($string) > 200) {

    // truncate string
    $stringCut = substr($string, 0, 100);
    $endPoint = strrpos($stringCut, ' ');

    //if the string doesn't contain any space then it will cut without word basis.
    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
    $string .= '...';
}
echo $string;
?>
                            </div>
                                <div class="d-flex justify-content-end">
                                    <i><?=$item->name;?></i>
                                </div>
                            </div>
                            <div class="overlay">
                                <div class="card-body">
                                    <div class="mb-4"><i><?=date('d F Y', strtotime($item->created_at));?></i></div>
                                    <a class="text-dark"
                                       href="<?=base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug?>">
                                        <h5><b><?=$item->title;?></b></h5>
                                    </a>
                                    <?php
$string = strip_tags($item->content);
if (strlen($string) > 500) {

    // truncate string
    $stringCut = substr($string, 0, 200);
    $endPoint = strrpos($stringCut, ' ');

    //if the string doesn't contain any space then it will cut without word basis.
    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
    $string .= '...';
}
echo $string;
?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
    <!-- end : content article -->
</div>
