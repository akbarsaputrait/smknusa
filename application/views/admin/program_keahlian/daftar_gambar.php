<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>
            <div class="card">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="btn-group">
                            <a href="<?= base_url() . 'admin/program_keahlian/list/' . $class_type ?>"
                               class="btn btn-outline-danger">Kembali</a>
                            <button type="button" data-toggle="modal" data-target="#modal_tambah_gambar"
                                    class="btn btn-outline-primary"><i
                                        class="icon icon-add"></i> Tambah Gambar
                            </button>
                        </div>
                    </div>
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Gambar</th>
                            <th>Tanggal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (is_null($image)) {
                            echo '<div class="alert alert-danger">Data tidak tersedia</div>';
                        } else {
                            $x = 1;
                            foreach ($image as $item):?>
                                <tr>
                                    <td><?= $x++; ?></td>
                                    <td>
                                        <img class="lazy"
                                             data-src="<?= base_url() . 'uploads/img/jurusan/' . $item->image; ?>"
                                             width="130">
                                    </td>
                                    <td><?= date('d F Y', strtotime($item->created_at)); ?></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="">
                                            <button type="button" data-toggle="modal" data-target="#modal_info_gambar"
                                                    class="btn btn-outline-primary"
                                                    data-id="<?= $item->id; ?>"
                                                    data-class="<?= $class_type; ?>"
                                                    data-class_id="<?= $class_id; ?>"><i class="icon icon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-outline-danger"
                                                    id="delete_gambar_jurusan"
                                                    data-id="<?= $item->id; ?>"
                                                    data-class="<?= $class_type; ?>"
                                                    data-class_id="<?= $class_id; ?>"><i class="icon icon-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- MODAL INFO Gambar -->
<div class="modal fade" id="modal_info_gambar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark border-0">
                <h5 class="modal-title text-white" id="">Keterangan Gambar</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart(base_url('admin/program_keahlian/update_post')); ?>
                <div class="form-group row">
                    <div class="col-md-12 d-flex justify-content-center align-items-center">
                        <img src="" width="130" id="preview_gambar" alt="">
                    </div>
                </div>
                <div class="form-group">
                    <input type="file" class="form-control" name="photo_class" id="input_gambar">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<!-- MODAL TAMBAH GAMBAR-->
<div class="modal fade" id="modal_tambah_gambar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 1050px !important;">
        <div class="modal-content">
            <div class="modal-header bg-dark border-0">
                <h5 class="modal-title text-white" id="">Tambah Gambar</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="actions" class="row">
                    <div class="col-md-7">
                        <!-- The fileinput-button span is used to style the file input field as button -->
                        <span class="btn btn-success fileinput-button">
     						<i class="fa fa-plus"></i>
     						Add files...
    					</span>
                        <button type="submit" class="btn btn-primary start">
                            <i class="fa fa-upload"></i>
                            <span>Start upload</span>
                        </button>
                        <button type="reset" class="btn btn-danger cancel removeAll">
                            <i class="fa fa-ban-circle"></i>
                            <span>Cancel upload</span>
                        </button>
                        <pre>or Drop the file to here.</pre>
                    </div>
                    <div class="col-md-5">
                        <span class="fileupload-process">
                            <div id="total-progress" class="progress progress-striped active" role="progressbar"
                                 aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"></div>
                            <div class="progress-bar progress-bar-success" style="width:0%;"
                                 data-dz-uploadprogress></div>
                        </span>
                    </div>
                </div>
                <div class="table table-striped" class="files" id="previews">
                    <div id="template" class="file-row row">
                        <!-- This is used as the file preview template -->
                        <div class="col-md-2 d-flex align-items-center">
                            <span class="preview"><img data-dz-thumbnail/></span>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-6 info-file">
                                    <div>
                                        <p class="name" data-dz-name></p>
                                        <strong class="error text-danger" data-dz-errormessage></strong>
                                    </div>
                                    <div>
                                        <p class="size" data-dz-size></p>
                                        <div class="progress progress-striped active" role="progressbar"
                                             aria-valuemin="0"
                                             aria-valuemax="100" aria-valuenow="0">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"
                                                 data-dz-uploadprogress></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 info-file">
                                    <button class="btn btn-primary start">
                                        <i class="fa fa-upload"></i>
                                        <span>Start</span>
                                    </button>
                                    <button data-dz-remove class="btn btn-danger cancel">
                                        <i class="fa fa-ban-circle"></i>
                                        <span>Cancel</span>
                                    </button>
                                    <p data-dz-remove class="delete">
                                        <i class="fa fa-check"></i>
                                        <span class="alert alert-success">Finish</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var class_id = <?= $class_id;?>;
</script>