<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>

            <div class="card">
                <div class="card-body">
                    <div class="row justify-content-center">
                        <button type="button" data-toggle="modal" data-target="#modal_tambah_jurusan"
                                class="btn btn-outline-primary"><i
                                    class="icon icon-add"></i> Tambah Jurusan
                        </button>
                    </div>
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Jurusan</th>
                            <th>Keterangan</th>
                            <th>Sejak Tahun</th>
                            <th>Tanggal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (is_null($keahlian)) {
                            echo '<div class="alert alert-danger">Data tidak tersedia</div>';
                        } else {
                            $x = 1;
                            foreach ($keahlian as $item): ?>
                                <tr>
                                    <td><?= $x++; ?></td>
                                    <td><?= $item->name; ?></td>
                                    <td>
                                        <?php
                                        $string = strip_tags($item->content);
                                        if (strlen($string) > 200) {

                                            // truncate string
                                            $stringCut = substr($string, 0, 100);
                                            $endPoint = strrpos($stringCut, ' ');

                                            //if the string doesn't contain any space then it will cut without word basis.
                                            $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                            $string .= '...';
                                        }
                                        echo $string;
                                        ?>
                                    </td>
                                    <td><?= $item->since; ?></td>
                                    <td><?= date('d F Y', strtotime($item->created_at)); ?></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="">
                                            <a class="btn btn-outline-success"
                                               href="<?= base_url() . 'admin/program_keahlian/image/' . $item->type . '/' . $item->id ?>">
                                                Lihat Gambar
                                            </a>
                                            <button type="button" data-toggle="modal" data-target="#modal_info_jurusan"
                                                    data-id="<?= $item->id; ?>"
                                                    data-class="<?= $item->type; ?>"
                                                    class="btn btn-outline-primary"><i
                                                        class="icon icon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-outline-danger"
                                                    id="delete_info_jurusan"
                                                    data-id="<?= $item->id; ?>"
                                                    data-class="<?= $item->type; ?>"><i class="icon icon-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- MODAL INFO Gambar -->
<div class="modal fade" id="modal_info_jurusan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 812px;">
        <div class="modal-content">
            <div class="modal-header bg-dark border-0">
                <h5 class="modal-title text-white" id="">Keterangan Jurusan</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open(base_url('admin/program_keahlian/update_info_jurusan/')); ?>
                <div class="form-group">
                    <label for="">Jurusan</label>
                    <input type="text" class="form-control" id="info_jurusan" name="jurusan" autofocus>
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" id="konten_blog" required></textarea>
                </div>
                <div class="form-group">
                    <label for="">Sejak Tahun</label>
                    <input type="text" class="form-control" id="info_sejak" name="sejak_tahun">
                </div>
                <div class="form-group">
                    <label for="">Tanggal</label>
                    <input type="text" class="form-control" id="info_tanggal" readonly>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<!-- MODAL TAMBAH JURUSAN-->
<div class="modal fade" id="modal_tambah_jurusan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark border-0">
                <h5 class="modal-title text-white" id="">Tambah Jurusan</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open(base_url('admin/program_keahlian/add_jurusan/' . $keahlian[0]->type . '')); ?>
                <div class="form-group">
                    <label for="">Jurusan</label>
                    <input type="text" class="form-control" id="info_jurusan" name="jurusan" autofocus>
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <input type="text" class="form-control" id="info_keterangan" name="keterangan">
                </div>
                <div class="form-group">
                    <label for="">Sejak Tahun</label>
                    <input type="text" class="form-control" id="info_sejak" name="sejak_tahun">
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
