<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card p-4">
                <div class="card-body d-flex justify-content-between align-items-center">
                    <div>
            <span class="h4 d-block font-weight-normal mb-2">
              <?php echo $total_blog; ?>
            </span>
                        <span class="font-weight-light">Total Blog</span>
                    </div>

                    <div class="h2 text-muted">
                        <i class="icon icon-notebook"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 dash-blog-list">
            <div class="card">
                <div class="card-header bg-danger">
                    <b>Blog Terbaru</b>
                </div>

                <div class="list-group">
                    <?php
                    if (is_null($blog)) {
                        echo '<div class="alert alert-danger m-0">Data tidak tersedia</div>';
                    } else {
                        foreach ($blog as $item) : ?>
                            <a href="<?= base_url('admin/blog/detail/' . $item['id'] . '-' . $item['slug']) ?>"
                               class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <h6 class="mb-1">
                                        <strong>
                                            <?= $item['title'] ?>
                                        </strong>
                                    </h6>
                                    <small>
                                        <?= date('d-M-Y', strtotime($item['created_at'])) ?>
                                    </small>
                                </div>
                                <small>
                                    <?php
                                    $string = strip_tags($item["content"]);
                                    if (strlen($string) > 200) {

                                        // truncate string
                                        $stringCut = substr($string, 0, 100);
                                        $endPoint = strrpos($stringCut, ' ');

                                        //if the string doesn't contain any space then it will cut without word basis.
                                        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                        $string .= '...';
                                    }
                                    echo $string;
                                    ?>
                                </small>
                            </a>
                        <?php endforeach;
                    } ?>
                </div>
                <div class="card-footer text-dark text-right">
                    <b><a href="<?= base_url() . 'admin/blog' ?>">Lihat Selengkapnya <i
                                    class="fa fa-arrow-right"></i></a></b>
                </div>
            </div>
        </div>
        <div class="col-md-6 dash-news-list">
            <div class="card">
                <div class="card-header bg-primary">
                    <b>Pengumuman Terbaru</b>
                </div>

                <div class="list-group">
                    <?php
                    if (is_null($pengumuman)) {
                        echo ' <div class="alert alert-danger m-0">Data tidak tersedia</div>';
                    } else {
                        foreach ($pengumuman as $item) : ?>
                            <a href="<?= base_url('admin/pengumuman/detail/' . $item['id'] . '-' . $item['slug']) ?>"
                               class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <h6 class="mb-1">
                                        <strong>
                                            <?= $item['title'] ?>
                                        </strong>
                                    </h6>
                                    <small>
                                        <?= date('d-M-Y', strtotime($item['created_at'])) ?>
                                    </small>
                                </div>
                                <small>
                                    <?php
                                    $string = strip_tags($item["content"]);
                                    if (strlen($string) > 200) {

                                        // truncate string
                                        $stringCut = substr($string, 0, 100);
                                        $endPoint = strrpos($stringCut, ' ');

                                        //if the string doesn't contain any space then it will cut without word basis.
                                        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                        $string .= '...';
                                    }
                                    echo $string;
                                    ?>
                                </small>
                            </a>
                        <?php endforeach;
                    } ?>
                </div>
                <div class="card-footer text-dark text-right">
                    <b><a href="<?= base_url() . 'admin/pengumuman' ?>">Lihat Selengkapnya <i
                                    class="fa fa-arrow-right"></i></a></b>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-warning text-white">
                    <b>Daftar Alumni</b>
                    <small class="card-subtitle">Belum terverifikasi</small>
                </div>

                <div class="list-group">
                    <?php
                    if (is_null($alumni)) {
                        echo ' <div class="alert alert-danger m-0">Data tidak tersedia</div>';
                    } else {
                        foreach ($alumni as $item) : ?>
                            <a class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between align-items-center">
                                    <h6 class="">
                                        <strong>
                                            <?= $item->name ?>
                                        </strong>
                                    </h6>
                                    <div class="btn-group">
                                        <button class="btn btn-info btn-raised mr-1"
                                                data-id="<?= $item->id; ?>"
                                                data-toggle="modal"
                                                data-target="#infoAlumni">
                                            <i class="icon icon-pencil m-0"></i> Edit
                                        </button>
                                        <button class="btn btn-danger btn-raised"
                                                id="delete_alumni"
                                                data-id="<?= $item->id; ?>">
                                            <i class="icon icon-trash m-0"></i> Hapus
                                        </button>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach;
                    } ?>
                </div>
                <div class="card-footer text-dark text-right">
                    <b><a href="<?= base_url() . 'admin/pengumuman' ?>">Lihat Selengkapnya <i
                                    class="fa fa-arrow-right"></i></a></b>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    <b><i class="fa fa-calendar" aria-hidden="true"></i> Agenda Terbaru</b>
                </div>

                <?php
                foreach ($agenda as $item):
                    ?>
                    <div class="list-group p-0">
                        <a href="<?= base_url() . 'admin/agenda'; ?>"
                           class="list-group-item list-group-item-action flex-column align-items-start">
                            <div class="d-flex w-100 justify-content-between">
                                <h6 class="mb-1 text-left">
                                    <strong>
                                        <?= $item->title; ?>
                                    </strong>
                                </h6>
                                <small>
                                    <?= date('d F Y', strtotime($item->start_date)); ?>
                                </small>
                            </div>
                        </a>
                    </div>
                <?php
                endforeach;
                ?>

                <div class="card-footer text-dark text-right">
                    <b><a href="<?= base_url() . 'event' ?>">Lihat Selengkapnya <i
                                    class="fa fa-arrow-right"></i></a></b>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="infoAlumni" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Info Alumni</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/alumni/add', array('id' => 'edit')); ?>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="name" class="form-control" placeholder="" id="info_nama">
                </div>

                <div class="form-group">
                    <label>Angkatan/Lulusan</label>
                    <input type="text" name="graduate" class="form-control"
                           placeholder="" id="info_lulusan">
                </div>

                <div class="form-group">
                    <label>Kuliah</label>
                    <input type="text" name="college" class="form-control"
                           placeholder="" id="info_kuliah">
                </div>

                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" name="work" class="form-control"
                           placeholder="" id="info_pekerjaan">
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="address" class="form-control"
                           placeholder="" id="info_alamat">
                </div>

                <div class="form-group">
                    <label>No. Telp</label>
                    <input type="text" name="phone_jumber" class="form-control"
                           placeholder="" id="info_telp">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control"
                           placeholder="" id="info_email">
                </div>

                <div class="form-group">
                    <label>Pesan</label>
                    <input type="text" name="message" class="form-control"
                           placeholder="" id="info_pesan">
                </div>

                <div class="form-group">
                    <label class="control-label" for="post_status">Status</label>
                    <select name="status" class="form-control input-sm"
                            id="info_status" required>
                        <option value="publish">Diterbitkan</option>
                        <option value="draft">Konsep</option>
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-raised btn-block">
                        <i class="icon icon-cursor"></i> Simpan
                    </button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>