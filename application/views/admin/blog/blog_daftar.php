<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('failed')) {
                echo '<div class="alert alert-danger notif">Blog Gagal Disimpan!</div>';
            } else if ($this->session->flashdata('success')) {
                echo '<div class="alert alert-success notif">Blog Berhasil Disimpan!</div>';
            } ?>
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Penulis</th>
                            <th>Status</th>
                            <th>Tanggal / Waktu</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (is_null($blog)) {
                            echo '<div class="alert alert-danger">Data tidak tersedia</div>';
                        } else {
                            $x = 1;
                            foreach ($blog as $item):?>
                                <tr>
                                    <td><?= $x++; ?></td>
                                    <td><?= $item->title; ?></td>
                                    <td><?= $item->name; ?></td>
                                    <td><?php echo ($item->status == 'draft') ? '<h5><span class="badge badge-warning">Draft</span></h5>' : '<h5><span class="badge badge-primary">Publish</span></h5>'; ?></td>
                                    <td><?= date('d F Y', strtotime($item->created_at)); ?></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="">
                                            <a href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug; ?>"
                                               class="btn btn-outline-success" title="Lihat Blog" target="_blank"><i
                                                        class="icon icon-eye"></i></a>
                                            <a href="<?= base_url() . 'admin/' . $item->type . '/detail/' . $item->id . '-' . $item->slug; ?>"
                                               class="btn btn-outline-primary"><i class="icon icon-pencil"></i></a>
                                            <button type="button" class="btn btn-outline-danger" id="delete_blog"
                                                    data-slug="<?= $item->slug; ?>" title="Hapus Blog"><i
                                                        class="icon icon-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>