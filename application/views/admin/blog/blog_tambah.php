<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <?= form_open_multipart(base_url('admin/blog/add_post/blog')); ?>
    <div class="row">
        <div class="col-md-8">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>
            <div class="card">
                <div class="card-body">
                    <div class="form-group mb-4">
                        <div class="CoverImage FlexEmbed FlexEmbed--2by1 cover-blog"
                             style="background-image:url(<?php echo base_url() ?>assets/themes/default/img/photo.png)"></div>
                    </div>
                    <div class="form-group">
                        <label>Cover Blog</label>
                        <div class="input-group">
                            <input type="file" name="cover_post" class="form-control-file" id="post_image"
                                   accept="image/*">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="judul_blog"
                               placeholder="Judul Blog" required>
                    </div>
                    <div class="form-group">
                        <textarea name="konten_blog" id="konten_blog" required></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <!-- Kateogri -->
            <div class="card">
                <div class="card-header bg-light">
                    <b>Kategori</b>
                </div>
                <div class="card-body">
                    <input type="text" class="form-control" name="kategori_blog"
                           id="kategori_blog">
                </div>
            </div>

            <!-- Tags -->
            <div class="card">
                <div class="card-header bg-light">
                    <b>Tags</b>
                </div>
                <div class="card-body">
                    <input type="text" class="form-control" name="tags_blog" id="tags_blog">
                </div>
            </div>

            <!-- Pengaturan Blog -->
            <div class="card">
                <div class="card-header bg-light">
                    <b>Pengaturan</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label" for="post_status">Status</label>
                        <select name="post_status" class="form-control input-sm"
                                id="post_status" required>
                            <option value="publish">Diterbitkan</option>
                            <option value="draft">Konsep</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" id="submit" class="btn btn-outline-primary btn-block">
                            <i class="icon icon-cursor"></i> Simpan
                        </button>
                        <a href="<?= base_url('admin/blog');?>" class="btn btn-outline-danger btn-block">
                            <i class="icon icon-close"></i> Batal
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= form_close();?>
</div>
</div>