<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <?= form_open_multipart(base_url('admin/blog/add_post/pengumuman')); ?>
    <div class="row">
        <div class="col-md-8">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <input type="text" class="form-control" name="judul_blog"
                               placeholder="Judul Pengumuman" required>
                    </div>
                    <div class="form-group">
                        <textarea name="konten_blog" id="konten_blog" required></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <!-- Kateogri -->
            <div class="card">
                <div class="card-header bg-light">
                    <b>Kategori</b>
                </div>
                <div class="card-body">
                    <input type="text" class="form-control" name="kategori_blog"
                           id="kategori_blog">
                </div>
            </div>

            <!-- Tags -->
            <div class="card">
                <div class="card-header bg-light">
                    <b>Tags</b>
                </div>
                <div class="card-body">
                    <input type="text" class="form-control" name="tags_blog" id="tags_blog">
                </div>
            </div>

            <!-- Pengaturan Blog -->
            <div class="card">
                <div class="card-header bg-light">
                    <b>Pengaturan</b>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="control-label" for="post_status">Status</label>
                        <select name="post_status" class="form-control input-sm"
                                id="post_status" required>
                            <option value="publish">Diterbitkan</option>
                            <option value="draft">Konsep</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="post_status">Terbit pada ?</label>
                        <div id="start_date">
                            <input name="start_date" class="form-control"
                                   type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="post_status">Sampai dengan ?</label>
                        <div id="end_date">
                            <input name="end_date" class="form-control"
                                   type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" id="submit" class="btn btn-outline-primary btn-block">
                            <i class="icon icon-cursor"></i> Simpan
                        </button>
                        <a href="<?= base_url('admin/pengumuman');?>" class="btn btn-outline-danger btn-block">
                            <i class="icon icon-close"></i> Batal
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= form_close(); ?>
</div>
