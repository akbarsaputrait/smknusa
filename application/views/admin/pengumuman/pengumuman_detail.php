<?php foreach ($post as $item): ?>
    <div class="container-fluid">
        <h4><?= $sub . $item->title; ?> </h4>
        <?php echo form_open_multipart(base_url('admin/blog/add_post/' . $item->type . '/' . $item->id . '-' . $item->slug . '')); ?>
        <div class="row">
            <div class="col-md-8">
                <?php if ($this->session->flashdata('failed')) {
                    echo $this->session->flashdata('failed');
                } else if ($this->session->flashdata('success')) {
                    echo $this->session->flashdata('success');
                } ?>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <input type="text" class="form-control" name="judul_blog"
                                   value="<?= $item->title; ?> ">
                        </div>
                        <div class="form-group">
                        <textarea name="konten_blog"
                                  id="konten_blog"><?= $item->content; ?> </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!-- Kateogri -->
                <div class="card">
                    <div class="card-header bg-light">
                        <b>Kategori</b>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="kategori_blog"
                                       id="kategori_blog" value="<?= $item->categories; ?> ">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Tags -->
                <div class="card">
                    <div class="card-header bg-light">
                        <b>Tags</b>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="tags_blog" id="tags_blog"
                                       value="<?= $item->tags; ?> ">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Pengaturan Blog -->
                <div class="card">
                    <div class="card-header bg-light">
                        <b>Pengaturan</b>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label" for="post_status">Status</label>
                            <select name="post_status" class="form-control input-sm"
                                    id="post_status">
                                <option value="publish" <?php if ($item->status == 'publish') {
                                    echo 'selected';
                                } ?>>Diterbitkan
                                </option>
                                <option value="draft" <?php if ($item->status == 'draft') {
                                    echo 'selected';
                                } ?>>Konsep
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="post_status">Terbit pada ?</label>
                            <div id="start_date">
                                <input data-date="<?= date('Y-m-d', strtotime($item->start_date));?>" name="start_date" class="form-control"
                                       type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="post_status">Sampai dengan ?</label>
                            <div id="end_date">
                                <input data-date="<?= date('Y-m-d', strtotime($item->end_date));?>" name="end_date" class="form-control"
                                       type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" id="submit" class="btn btn-outline-primary btn-block">
                                <i class="icon icon-cursor"></i> Simpan
                            </button>
                            <a href="<?= base_url() ?>admin/pengumuman" class="btn btn-outline-danger btn-block">
                                <i class="icon icon-close"></i> Batal
                            </a>
                        </div>
                    </div>
                </div>

                <!-- INFORMASI BLOG -->
                <div class="card">
                    <div class="card-header bg-light">
                        <b>Keterangan Blog</b>
                    </div>
                    <div class="card-body">
                        <ul class="nav">
                            <li class="nav-item"><b>Dibuat Oleh</b> : <?= $item->name; ?></li>
                            <li class="nav-item"><b>Pada Tanggal</b>
                                : <?= date('d F Y', strtotime($item->created_at)); ?> </li>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    </div>
    <!-- END CONTENT -->
<?php endforeach; ?>