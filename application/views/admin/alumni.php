<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-center">
                            <button class="btn btn-info" data-toggle="modal" data-target="#tambahAlumni">
                                <i class="icon icon-plus"></i> Tambah Alumni
                            </button>
                        </div>
                    </div>
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Angkatan/Lulus</th>
                            <th>Kuliah</th>
                            <th>Pekerjaan</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $x = 1;
                        foreach ($alumni as $item):
                            ?>
                            <tr>
                                <td><?= $x++; ?></td>
                                <td><?= $item->name; ?></td>
                                <td><?= $item->graduate; ?></td>
                                <td><?= $item->college; ?></td>
                                <td><?= $item->work; ?></td>
                                <td>
                                    <?php
                                        if ($item->created_by == NULL) {
                                            echo '<h5><span class="badge badge-warning">Unverified</span></h5>';
                                        } elseif ($item->status == 'publish') {
                                            echo '<h5><span class="badge badge-primary">Publish</span></h5>';
                                        } else {
                                            echo '<h5><span class="badge badge-warning">Draft</span></h5>';
                                        }
                                    ?>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-outline-info"
                                                data-id="<?= $item->id; ?>"
                                                data-toggle="modal"
                                                data-target="#infoAlumni">
                                            <i class="icon icon-pencil"></i>
                                        </button>
                                        <button class="btn btn-outline-danger"
                                                id="delete_alumni"
                                                data-id="<?= $item->id; ?>">
                                            <i class="icon icon-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tambahAlumni" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Tambah Alumni</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/alumni/add'); ?>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
                </div>

                <div class="form-group">
                    <label>Angkatan/Lulusan</label>
                    <input type="text" name="graduates" class="form-control"
                           placeholder="Tahun">
                </div>

                <div class="form-group">
                    <label>Kuliah</label>
                    <input type="text" name="college" class="form-control"
                           placeholder="Kuliah">
                </div>

                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" name="work" class="form-control"
                           placeholder="Pekerjaan">
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="address" class="form-control"
                           placeholder="Alamat">
                </div>

                <div class="form-group">
                    <label>No. Telp</label>
                    <input type="text" name="phone_number" class="form-control"
                           placeholder="08xxxxxxxxx">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control"
                           placeholder="">
                </div>

                <div class="form-group">
                    <label>Pesan</label>
                    <input type="text" name="message" class="form-control"
                           placeholder="Pesan & kesan">
                </div>

                <div class="form-group">
                    <label class="control-label" for="post_status">Status</label>
                    <select name="status" class="form-control input-sm"
                            id="post_status" required>
                        <option value="publish">Diterbitkan</option>
                        <option value="draft">Konsep</option>
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="icon icon-cursor"></i> Simpan
                    </button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="infoAlumni" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Info Alumni</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/alumni/add', array('id' => 'edit')); ?>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" id="info_nama">
                </div>

                <div class="form-group">
                    <label>Angkatan/Lulusan</label>
                    <input type="text" name="graduate" class="form-control"
                           placeholder="Tahun" id="info_lulusan">
                </div>

                <div class="form-group">
                    <label>Kuliah</label>
                    <input type="text" name="college" class="form-control"
                           placeholder="Kuliah" id="info_kuliah">
                </div>

                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" name="work" class="form-control"
                           placeholder="Pekerjaan" id="info_pekerjaan">
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="address" class="form-control"
                           placeholder="Alamat" id="info_alamat">
                </div>

                <div class="form-group">
                    <label>No. Telp</label>
                    <input type="text" name="phone_jumber" class="form-control"
                           placeholder="08xxxxxxxxx" id="info_telp">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control"
                           placeholder="" id="info_email">
                </div>

                <div class="form-group">
                    <label>Pesan</label>
                    <input type="text" name="message" class="form-control"
                           placeholder="Pesan & kesan" id="info_pesan">
                </div>

                <div class="form-group">
                    <label class="control-label" for="post_status">Status</label>
                    <select name="status" class="form-control input-sm"
                            id="info_status" required>
                        <option value="publish">Diterbitkan</option>
                        <option value="draft">Konsep</option>
                    </select>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="icon icon-cursor"></i> Simpan
                    </button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
