<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div id="actions" class="row">
        <?php if ($this->session->flashdata('failed')) {
            echo $this->session->flashdata('failed');
        } else if ($this->session->flashdata('success')) {
            echo $this->session->flashdata('success');
        } ?>
        <div class="col-md-7">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn btn-success fileinput-button">
     						<i class="fa fa-plus"></i>
     						Add files...
    					</span>
            <button type="submit" class="btn btn-primary start">
                <i class="fa fa-upload"></i>
                <span>Start upload</span>
            </button>
            <button type="reset" class="btn btn-danger cancel removeAll">
                <i class="fa fa-ban-circle"></i>
                <span>Cancel upload</span>
            </button>
            <a href="<?php echo base_url() ?>admin/guru" class="btn btn-warning">
                Daftar Galeri
            </a>
            <pre>or Drop the file to here.</pre>
        </div>
        <div class="col-md-5">
            <span class="fileupload-process">
                <div id="total-progress" class="progress progress-striped active" role="progressbar"
                     aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"></div>
                <div class="progress-bar progress-bar-success" style="width:0%;"
                     data-dz-uploadprogress></div></span>
        </div>
    </div>
    <div class="table table-striped" class="files" id="previews">
        <div id="template" class="file-row row">
            <!-- This is used as the file preview template -->
            <div class="col-md-2 d-flex align-items-center">
                <span class="preview"><img data-dz-thumbnail/></span>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6 info-file">
                        <div>
                            <p class="name" data-dz-name></p>
                            <strong class="error text-danger" data-dz-errormessage></strong>
                        </div>
                        <div>
                            <p class="size" data-dz-size></p>
                            <div class="progress progress-striped active" role="progressbar"
                                 aria-valuemin="0"
                                 aria-valuemax="100" aria-valuenow="0">
                                <div class="progress-bar progress-bar-success" style="width:0%;"
                                     data-dz-uploadprogress></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 info-file">
                        <button class="btn btn-primary start">
                            <i class="fa fa-upload"></i>
                            <span>Start</span>
                        </button>
                        <button data-dz-remove class="btn btn-danger cancel">
                            <i class="fa fa-ban-circle"></i>
                            <span>Cancel</span>
                        </button>
                        <p data-dz-remove class="delete">
                            <i class="fa fa-check"></i>
                            <span class="alert alert-success">Finish</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

