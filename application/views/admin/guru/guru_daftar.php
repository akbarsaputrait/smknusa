<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Foto</th>
                            <th>Nama</th>
                            <th>Profesi</th>
                            <th>Tanggal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (is_null($guru)) {
                            echo '<div class="alert alert-danger">Data tidak tersedia</div>';
                        } else {
                            $x = 1;
                            foreach ($guru as $item):
                                ?>
                                <tr>
                                    <td><?= $x++; ?></td>
                                    <td>
                                        <img class="lazy" width="130"
                                             data-src="<?= base_url('uploads/img/guru/' . $item->image); ?>">
                                    </td>
                                    <td><?= $item->name; ?></td>
                                    <td><?= $item->profession; ?></td>
                                    <td><?= date('d F Y', strtotime($item->created_at)); ?></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="">
                                            <button type="button" data-toggle="modal" data-target="#modal_info_guru"
                                                    data-id="<?= $item->id; ?>" class="btn btn-outline-primary"><i
                                                        class="icon icon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-outline-danger" id="delete_guru"
                                                    data-id="<?= $item->id; ?>"><i class="icon icon-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            endforeach;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- MODAL INFO Gambar -->
<div class="modal fade" id="modal_info_guru" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark border-0">
                <h5 class="modal-title text-white" id="">Keterangan Gambar</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart(base_url('admin/guru/update_post')); ?>
                <div class="form-group row">
                    <div class="col-md-12 d-flex justify-content-center align-items-center">
                        <img src="" width="130" id="preview_gambar" alt="">
                    </div>
                </div>
                <div class="form-group">
                    <input type="file" class="form-control" name="photo_teacher" id="input_gambar">
                </div>
                <div class="form-group">
                    <label for="">Nama Guru</label>
                    <input type="text" class="form-control" id="nama_guru" name="teacher_name">
                </div>
                <div class="form-group">
                    <label for="">Profesi</label>
                    <input type="text" class="form-control" id="nama_profesi" name="teacher_profession">
                </div>
                <div class="form-group">
                    <label for="">Sejak Tahun</label>
                    <input type="text" class="form-control" id="tahun" name="teacher_year">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" class="form-control" id="email" name="teacher_email">
                </div>
                <div class="form-group">
                    <label for="">Tanggal</label>
                    <input type="text" class="form-control" id="info_tanggal" readonly>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
