<div class="container-fluid">
    <div class="row">
        <div class="col-md-2" role="tablist">
            <div class="list-group">
                <a data-toggle="tab" href="#profile" role="tab" class="list-group-item active">Profile</a>
                <a data-toggle="tab" href="#password" role="tab" class="list-group-item">Change Password</a>
            </div>
        </div>
        <div class="col-md-10">
            <div class="tab-content" style="border: none; color: black !important;">
                <div class="tab-pane fade show active" id="profile" role="tabpanel" style="padding: 0;">
                    <div class="card">
                        <div class="card-header bg-light">
                            <div>Profile Information</div>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <div class="row d-flex justify-content-center">
                                    <div class="col-md-6">
                                        <?php if ($this->session->flashdata('failed')) {
                                            echo $this->session->flashdata('failed');
                                        } else if ($this->session->flashdata('success')) {
                                            echo $this->session->flashdata('success');
                                        } ?>
                                        <?= form_open_multipart(base_url('admin/profile/update_users/')); ?>
                                        <div class="form-group d-flex justify-content-center align-items-center">
                                            <img src="<?php echo base_url('uploads/img/photo_profile/' . $this->user['photo']) ?>"
                                                 width="150" id="preview_gambar" alt="">
                                        </div>
                                        <div class="form-group">
                                            <input type="file" name="photo" class="form-control" id="input_gambar">
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="">Username</label>
                                            <input type="text" class="form-control" name="username"
                                                   value="<?= $this->user['username']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="">Email</label>
                                            <input type="email" class="form-control" name="email"
                                                   value="<?= $this->user['email']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="">Name</label>
                                            <input type="text" class="form-control" name="name"
                                                   value="<?= $this->user['name']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-outline-primary btn-block">Update
                                            </button>
                                        </div>
                                        <?= form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="password" role="tabpanel" style="border: none; padding: 0;">
                    <div class="card" id="profile">
                        <div class="card-header bg-light">
                            Change Password
                        </div>
                        <div class="card-body mb-5">
                            <?= form_open(base_url('admin/profile/update_password/'), 'id = "change_password"'); ?>
                            <div class="form-group">
                                <label for="" class="">Current Pasword</label>
                                <input type="password" class="form-control" name="current_password">
                            </div>
                            <div class="form-group">
                                <label for="" class="">New Password</label>
                                <input type="password" class="form-control" name="new_password">
                            </div>
                            <div class="form-group ">
                                <label for="" class="">Re-enter Password</label>
                                <input type="password" class="form-control" name="confirm_password">
                            </div>
                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary btn-block">Update
                                    Password
                                </button>
                            </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<!--<script>-->
<!--    $(function () {-->
<!--        $('input[name="photo"]').change(function () {-->
<!--            readURL(this);-->
<!--        });-->
<!---->
<!--        function readURL(input) {-->
<!--            if (input.files && input.files[0]) {-->
<!--                var reader = new FileReader();-->
<!---->
<!--                reader.onload = function (e) {-->
<!--                    $('#photo_profile').attr('src', e.target.result);-->
<!--                }-->
<!---->
<!--                reader.readAsDataURL(input.files[0]);-->
<!--            }-->
<!--        }-->
<!--    });-->
<!---->
<!--</script>-->
