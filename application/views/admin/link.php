<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 d-flex justify-content-center">
                            <button class="btn btn-info" data-toggle="modal" data-target="#tambahLink">
                                <i class="icon icon-plus"></i> Tambah Tautan
                            </button>
                        </div>
                    </div>
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Tautan</th>
                            <th>Tanggal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $x = 1;
                        foreach ($links as $item):
                            ?>
                            <tr>
                                <td><?= $x++ ?></td>
                                <td><?= $item->title; ?></td>
                                <td><?= $item->url; ?></td>
                                <td><?= date('d F Y', strtotime($item->created_at)); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-outline-info"
                                                data-id="<?= $item->id; ?>"
                                                data-toggle="modal"
                                                data-target="#infoLink">
                                            <i class="icon icon-pencil"></i>
                                        </button>
                                        <button class="btn btn-outline-danger"
                                                id="delete_tautan"
                                                data-id="<?= $item->id; ?>">
                                            <i class="icon icon-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="tambahLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Tambah Link Eksternal</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/link/add'); ?>
                <div class="form-group">
                    <label>Judul</label>
                    <input type="text" name="title" class="form-control" placeholder="Title">
                </div>

                <div class="form-group">
                    <label>Tautan</label>
                    <input type="text" name="url" class="form-control"
                           placeholder="www.example.com">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="icon icon-cursor"></i> Simpan
                    </button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="infoLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Info Link Eksternal</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('admin/link/edit', array('id' => 'edit')); ?>
                <div class="form-group">
                    <label>Judul</label>
                    <input type="text" name="title" id="title_link" class="form-control">
                </div>

                <div class="form-group">
                    <label>Tautan</label>
                    <input type="text" name="url" id="url_link" class="form-control">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="icon icon-cursor"></i> Simpan
                    </button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
