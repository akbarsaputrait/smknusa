<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-12">
            <?php if ($this->session->flashdata('failed')) {
                echo $this->session->flashdata('failed');
            } else if ($this->session->flashdata('success')) {
                echo $this->session->flashdata('success');
            } ?>
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Foto</th>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th>Tanggal</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (is_null($fasilitas)) {
                            echo '<div class="alert alert-danger">Data tidak tersedia</div>';
                        } else {
                            $x = 1;
                            foreach ($fasilitas as $item):
                                ?>
                                <tr>
                                    <td><?= $x++; ?></td>
                                    <td>
                                        <img class="lazy" width="130"
                                             data-src="<?= base_url('uploads/img/fasilitas/' . $item->image); ?>">
                                    </td>
                                    <td><?= $item->nama_fasilitas; ?></td>
                                    <td><?= $item->content; ?></td>
                                    <td><?= date('d F Y', strtotime($item->created_at)); ?></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="">
                                            <button type="button" data-toggle="modal"
                                                    data-target="#modal_info_fasilitas"
                                                    data-id="<?= $item->id; ?>" class="btn btn-outline-primary"><i
                                                        class="icon icon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-outline-danger" id="delete_fasilitas"
                                                    data-id="<?= $item->id; ?>"><i class="icon icon-trash"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                            endforeach;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- MODAL INFO Gambar -->
<div class="modal fade" id="modal_info_fasilitas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark border-0">
                <h5 class="modal-title text-white" id="">Keterangan Gambar</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart(base_url('admin/fasilitas/update_post')); ?>
                <div class="form-group row">
                    <div class="col-md-12 d-flex justify-content-center align-items-center">
                        <img src="" width="130" id="preview_gambar" alt="">
                    </div>
                </div>
                <div class="form-group">
                    <input type="file" class="form-control" name="photo_fasilitas" id="input_gambar">
                </div>
                <div class="form-group">
                    <label for="">Nama Fasilitas</label>
                    <input type="text" class="form-control" id="fasilitas_nama" name="fasilitas_nama">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <input type="text" class="form-control" id="fasilitas_keterangan" name="fasilitas_keterangan">
                </div>
                <div class="form-group">
                    <label for="">Oleh</label>
                    <input type="text" class="form-control" id="info_penulis" readonly>
                </div>
                <div class="form-group">
                    <label for="">Tanggal</label>
                    <input type="text" class="form-control" id="info_tanggal" readonly>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
