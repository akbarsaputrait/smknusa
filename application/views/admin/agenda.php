<div class="container-fluid">
    <h4><?= $sub; ?></h4>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-light">
                    <b>Tambah Agenda</b>
                </div>

                <div class="card-body">
                    <form id="addEvent">
                        <div class="form-group">
                            <label>Judul Agenda</label>
                            <input type="text" name="title" id="" class="form-control" placeholder="Title">
                        </div>

                        <div class="form-group">
                            <label>Tautan</label>
                            <input type="text" name="url" id="" class="form-control"
                                   placeholder="www.example.com">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Mulai</label>
                            <div id="tgl_mulai">
                                <input placeholder="Start Date" name="start_date" class="form-control"
                                       type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Berakhir</label>
                            <div id="tgl_berakhir">
                                <input placeholder="End Date" name="end_date" class="form-control"
                                       type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Warna Latar</label>
                            <input type="color" name="background_color" id="" style="height: 2rem;"
                                   class="form-control" value="#000000">
                        </div>

                        <div class="form-group">
                            <label>Warna Text</label>
                            <input type="color" name="text_color" id="" style="height: 2rem;"
                                   class="form-control" value="#ffffff">
                        </div>

                        <div class="form-group">
                            <button id="submit" class="btn btn-primary btn-block">
                                <i class="icon icon-cursor"></i> Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-light">
                    <b>Kalender</b>
                </div>

                <div class="card-body">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table id="datatable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Tanggal</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $x = 1;
                        foreach ($agenda as $item):
                            ?>
                            <tr>
                                <td><?= $x++;?></td>
                                <td><?= $item->title;?></td>
                                <td><?= '<b>'. date('d F Y', strtotime($item->start_date)) . '</b> s/d <b>' . date('d F Y', strtotime($item->end_date)) . '</b>';?></td>
                            </tr>
                        <?php
                        endforeach;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Informasi Agenda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="updateEvent">
                    <div class="form-group">
                        <label>Judul Agenda</label>
                        <input type="text" name="info_title" id="show_judul_agenda" class="form-control"
                               placeholder="Title">
                    </div>

                    <div class="form-group">
                        <label>Tautan</label>
                        <input type="text" name="info_url" id="show_tautan_agenda" class="form-control"
                               placeholder="www.example.com">
                    </div>

                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <div id="tgl_mulai">
                            <input placeholder="Start Date" id="show_tgl_mulai_agenda" name="info_start_date"
                                   class="form-control"
                                   type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Berakhir</label>
                        <div id="tgl_berakhir">
                            <input placeholder="End Date" id="show_tgl_berakhir_agenda" name="info_end_date"
                                   class="form-control"
                                   type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Warna Background</label>
                        <input type="color" name="info_background_color" id="show_warna_bg" style="height: 2rem;"
                               class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Warna Text</label>
                        <input type="color" name="info_text_color" id="show_warna_text" style="height: 2rem;"
                               class="form-control">
                    </div>

                    <div class="form-group">
                        <button id="submit" class="btn btn-primary btn-block">
                            <i class="icon icon-cursor"></i> Simpan
                        </button>
                        <button id="delete_agenda" type="button" class="btn btn-danger btn-block">
                            <i class="icon icon-trash"></i> Hapus
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
