<div class="<?= $color; ?>">
    <div class="container">
        <div class="text-center text-white mb-4">
            <h1>
                <b><?= $title; ?></b>
            </h1>
        </div>
        <div class="card">
            <div class="card-header bg-light">
                <b>Kalender</b>
            </div>

            <div class="card-body">
                <div id="calendar"></div>
            </div>
        </div>

        <div class="list-blog mt-5">
            <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
            <div class="row">
                <?php foreach ($posts as $item): ?>
                    <div class="col-md-4 pb-3 text-left">
                        <div class="containera card">
                            <div class="CoverImage FlexEmbed FlexEmbed--2by1"
                                 style="background-image:url(<?= base_url() . 'uploads/img/cover_post/' . $item->image; ?>)"></div>
                            <div class="card-body" style="padding:20px 30px;">
                                <h6 class="">
                                    <b><?= $item->title; ?></b>
                                </h6>
                                <div class="">
                                    <?php
                                        $string = strip_tags($item->content);
                                        if (strlen($string) > 200) {

                                            // truncate string
                                            $stringCut = substr($string, 0, 100);
                                            $endPoint = strrpos($stringCut, ' ');

                                            //if the string doesn't contain any space then it will cut without word basis.
                                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                                            $string .= '...';
                                        }
                                        echo $string;
                                    ?>
                            </div>
                                <div class="d-flex justify-content-end">
                                    <i><?= $item->name; ?></i>
                                </div>
                            </div>
                            <div class="overlay">
                                <div class="card-body">
                                    <div class="mb-4"><i><?= date('d F Y', strtotime($item->created_at)); ?></i></div>
                                    <a class="text-dark"
                                       href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>">
                                        <h5><b><?= $item->title; ?></b></h5>
                                    </a>
                                    <?php
                                    $string = strip_tags($item->content);
                                    if (strlen($string) > 500) {

                                        // truncate string
                                        $stringCut = substr($string, 0, 200);
                                        $endPoint = strrpos($stringCut, ' ');

                                        //if the string doesn't contain any space then it will cut without word basis.
                                        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                        $string .= '...';
                                    }
                                    echo $string;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Informasi Agenda</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="updateEvent">
                    <div class="form-group">
                        <label>Judul Agenda</label>
                        <input type="text" name="info_title" id="show_judul_agenda" class="form-control"
                               placeholder="Title">
                    </div>

                    <div class="form-group">
                        <label>Tautan</label>
                        <input type="text" name="info_url" id="show_tautan_agenda" class="form-control"
                               placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <div id="tgl_mulai">
                            <input placeholder="Start Date" id="show_tgl_mulai_agenda" name="info_start_date"
                                   class="form-control"
                                   type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Berakhir</label>
                        <div id="tgl_berakhir">
                            <input placeholder="End Date" id="show_tgl_berakhir_agenda" name="info_end_date"
                                   class="form-control"
                                   type="text">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>