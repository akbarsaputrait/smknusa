<div class="<?= $color; ?>">
    <div class="container">
        <div class="text-center text-white mb-4">
            <h1><b><?= $title; ?></b></h1>
        </div>
        <!-- start : caraosel -->

        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="slider single-item">
                        <?php
                        foreach ($fasilitas as $item):
                            ?>
                            <div>
                                <div class="CoverImage FlexEmbed FlexEmbed--0by1"
                                     style='background-image:url(<?php echo '"' . base_url() . "uploads/img/fasilitas/" . $item->image . '"'; ?>);'></div>
                            </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                    <div class="card-body  bg-white p-4">
                        <h6>
                            <i>
                                <b>VISI</b>
                            </i>
                        </h6>
                        <p class="font--sz14">"Membentuk Tamatan yang beriman dan bertaqwa kepada Tuhan Yang Maha Esa,
                            berbudi pekerti luhur, berdaya
                            saing tinggi, peduli dan berbudaya lingkungan".</h6>
                        <h6>
                            <i>
                                <b>MISI</b>
                            </i>
                        </h6>
                        <p>
                        <ol>
                            <li>Mengembangkan disiplin diri, etos kerja yang tinggi, kreatif, inovetiv dan budaa santun.
                            </li>
                            <li>Mengembangkan budaya sekolah untuk melestarikan fungsi lingkungan, mencegah pencemaran
                                dan kerusakan lngkungan.
                            </li>
                            <li>Mengembangkan pola kemitraan industri yang berorientasi pada keseimbangan lingkungan
                                hidup.
                            </li>
                            <li>Mengembangkan sistem nilai sekolah berorientasi pada nilai industri.</li>
                            <li>Memahami perang dan fungsi masing-masing dalm pergaulan sesama warga sekolah dalam
                                suasana
                                kekeluargaan.
                            </li>
                            <li>Mampu memanfaatkan potensi wilayah untuk pengembangan sekolah sebagai pusat :</li>
                            <ol class="list-style-ol">
                                <li>Budaya</li>
                                <li>Pelatihan</li>
                                <li>Pengembangan jaringan Perawatan dan Perbaikan Industri</li>
                                <li>Produksi dan Jasa</li>
                                <li>Pendidikan Lingkungan Hdup</li>
                            </ol>
                        </ol>
                        </p>
                    </div>
                </div>
            </div
        </div>
    </div>

    <!-- start : caraosel -->


    <!-- start : content kepala sekolah -->
    <div class="container padding-content">
        <div class="content-profile">
            <div class="row">
                <div class="col-md-3 p-4">
                    <div class="text-center">
                        <img src="<?= base_url() . 'assets/themes/default/img/Avatar_black.png' ?>">
                        <h6 class="text-center"><br>Ir. Indra Jaya M.Pd</h6>
                        <p class="text-center font--sz12">Kepala Sekolah</p>
                    </div>
                </div>
                <div class="col-md-9 p-4">
                    <p>Selamat datang di website SMKN 1 Purwosari yang merupakan
                        lembaga pusat pendidikan dan juga sebagai
                        salah satu Pusat Pelatihan dan Pendidikan Keterampilan Terpadu (PPPKT) di Propinsi Jawa
                        Timur.
                        SMKN 1 Purwosari merupakan Sekolah Adiwiyata yang berada di Purwosari dimana tempatnya yang
                        sangat strategis dan mudah dijangkau, mempunyai Motto "Kepuasan anda, Prestasi kami" Segenap
                        keluarga besar SMKN 1 Purwosari senantiasa akan memberikan layanan yang prima bagi
                        masyarakat.
                        Selamat bergabung.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- end : content kepala sekolah -->
</div>

<!-- start : content location school -->
<div class="w-100 padding-content">
    <div class="text-center">
        <h4><b>Lokasi Sekolah</b></h4>
        <p>SMK Negeri 1 Purwosari berada di Jl. Raya <br> Purwosari dengan tempat yang staegis.</p>
    </div>
    <div class="col-md-12 maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.2077548929706!2d112.74621741432382!3d-7.767780394401745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7d3c5631acbf5%3A0xa71d9f205034b481!2sSMK+Negeri+1+Purwosari!5e0!3m2!1sid!2sid!4v1531316740482"
                frameborder="0" style="border:0;" width="100" height="450"
                allowfullscreen></iframe>
    </div>
</div>
<!-- end : content location school -->

<!-- start : content article -->
<div class="container">
    <div class="list-blog mt-5">
        <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
        <div class="row">
            <?php foreach ($post as $item): ?>
                <div class="col-md-4 pb-3 text-left">
                    <div class="containera card">
                        <div class="CoverImage FlexEmbed FlexEmbed--2by1"
                             style="background-image:url(<?= base_url() . 'uploads/img/cover_post/' . $item->image; ?>)"></div>
                        <div class="card-body" style="padding:20px 30px;">
                            <h6 class="">
                                <b><?= $item->title; ?></b>
                            </h6>
                            <div class="">
                                    <?php
$string = strip_tags($item->content);
if (strlen($string) > 200) {

    // truncate string
    $stringCut = substr($string, 0, 100);
    $endPoint = strrpos($stringCut, ' ');

    //if the string doesn't contain any space then it will cut without word basis.
    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
    $string .= '...';
}
echo $string;
?>
                            </div>
                            <div class="d-flex justify-content-end">
                                <i><?= $item->name; ?></i>
                            </div>
                        </div>
                        <div class="overlay">
                            <div class="card-body">
                                <div class="mb-4"><i><?= date('d F Y', strtotime($item->created_at)); ?></i></div>
                                <a class="text-dark"
                                   href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>">
                                    <h5><b><?= $item->title; ?></b></h5>
                                </a>
                                <?php
                                $string = strip_tags($item->content);
                                if (strlen($string) > 500) {

                                    // truncate string
                                    $stringCut = substr($string, 0, 200);
                                    $endPoint = strrpos($stringCut, ' ');

                                    //if the string doesn't contain any space then it will cut without word basis.
                                    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string .= '...';
                                }
                                echo $string;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<!-- end : content article -->
</div>
