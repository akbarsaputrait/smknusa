<div class="<?= $color; ?>">
    <div class="container">
        <div class="text-center text-white mb-4">
            <h1 class="h1-responsive">
                <b><?= $title; ?></b>
            </h1>
        </div>
        <div class="card">
            <div class="content-blog">
                <div class="row">
                    <?php
                    if (is_null($guru)) {
                        echo '<div class="alert alert-danger">Data tidak tersedia</div>';
                    } else {
                        foreach ($guru as $item): ?>
                            <div class="col-md-3 pb-3">
                                <div class="card">
                                    <div class="CoverImage FlexEmbed FlexEmbed--100 lazy"
                                         data-src="<?= base_url() . 'uploads/img/guru/' . $item->image; ?>">
                                        <h6 class="text-center text-white"><?= $item->name; ?></h6>
                                        <h6 class="text-center text-white font--sz11"><?= $item->profession; ?></h6>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                    } ?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                        <?= $pagination; ?>
                    </div>
            </div>
        </div>

        <div class="list-blog mt-5">
            <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
            <div class="row">
                <?php foreach ($posts as $item): ?>
                    <div class="col-md-4 pb-3 text-left">
                        <div class="containera card">
                            <div class="CoverImage FlexEmbed FlexEmbed--2by1"
                                 style="background-image:url(<?= base_url() . 'uploads/img/cover_post/' . $item->image; ?>)"></div>
                            <div class="card-body" style="padding:20px 30px;">
                                <h6 class="">
                                    <b><?= $item->title; ?></b>
                                </h6>
                                <div class="">
                                    <?php
                                        $string = strip_tags($item->content);
                                        if (strlen($string) > 200) {

                                            // truncate string
                                            $stringCut = substr($string, 0, 100);
                                            $endPoint = strrpos($stringCut, ' ');

                                            //if the string doesn't contain any space then it will cut without word basis.
                                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                                            $string .= '...';
                                        }
                                        echo $string;
                                    ?>
                            </div>
                                <div class="d-flex justify-content-end">
                                    <i><?= $item->name; ?></i>
                                </div>
                            </div>
                            <div class="overlay">
                                <div class="card-body">
                                    <div class="mb-4"><i><?= date('d F Y', strtotime($item->created_at)); ?></i></div>
                                    <a class="text-dark"
                                       href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>">
                                        <h5><b><?= $item->title; ?></b></h5>
                                    </a>
                                    <?php
                                    $string = strip_tags($item->content);
                                    if (strlen($string) > 500) {

                                        // truncate string
                                        $stringCut = substr($string, 0, 200);
                                        $endPoint = strrpos($stringCut, ' ');

                                        //if the string doesn't contain any space then it will cut without word basis.
                                        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                        $string .= '...';
                                    }
                                    echo $string;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
