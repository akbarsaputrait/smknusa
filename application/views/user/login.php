<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row justify-content-center pt-5">
    <div class="col-md-5">
        <div class="card p-4">
            <div class="card-header text-center text-uppercase h4 font-weight-light">
                Login
            </div>

            <div class="p-4">
                <?php echo form_open('', array('class'=>'form-signin')); ?>
                    <div class="form-group">
                        <label class="form-control-label">Username/Email</label>
                        <input type="text" name="username" class="form-control" autofocus>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Password</label>
                        <input type="password" name="password" class="form-control">
                    </div>

                    <div class="form-group">
                        <?= form_submit(array('name'=>'submit', 'class'=>'btn btn-primary btn-block active'), "Masuk"); ?>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
