<div class="<?=$color;?>">
    <div class="container">
        <div class="text-center text-white mb-4">
            <h1><b><?=$title;?></b></h1>
        </div>
        <!-- start : caraosel -->
        <div class="card padding-content">
            <div class="row block">
                <div class="col-md-7">
                    <div class="card h-100">
                        <div class="slider single-item m-0 h-100">
                            <div>
                                <div class="h-100">
                                    <div class="CoverImage FlexEmbed FlexEmbed--100 lazy h-100"
                                         data-src="<?=base_url() . 'assets/themes/default/img/photo.png'?>"></div>
                                </div>
                            </div>
                            <div>
                                <div class="h-100">
                                    <div class="CoverImage FlexEmbed FlexEmbed--100 lazy h-100"
                                         data-src="<?=base_url() . 'assets/themes/default/img/photo.png'?>"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 " style="padding:35px 50px 10px 30px;">
                    <p class="text-center font--sz16"><b>Program Kerja SMK Negeri 1 Purwosari</b></p><br>
                    <p><i><b>Program Unggulan</b></i></p>
                    <p>
                    <ol>
                        <li>Menjadi Sekolah Standart Nasional (SSN)</li>
                        <li>Mengembangkan Sikap dan Kompetensi Keagamaan</li>
                        <li>Mengembangkan Potensi Siswa Berbasis Multiple Intelligance</li>
                        <li>Mengembangkan Budaya Daerah</li>
                        <li>Mengembangkan Kemampuan Bahasa dan Teknologi Informasi</li>
                        <li>Meningkatkan Daya Serap Ke Perguruan Tinggi Favorit</li>
                    </ol>
                    </p>
                    <p><i><b>Program Sarana Pengembangan Prioritas</b></i></p>
                    <p>
                    <ol>
                        <li>Membangun 5 ruang kelas belajar dengan konstruksi bangunan 3 tingkat</li>
                        <li>Membangun 1 ruang belajar di lantai 2 gedung lama</li>
                        <li>Membangun ruang pengolah data</li>
                        <li>Pembangunan kantin siswa</li>
                        <li>Perbaikan dan pengecetan lapangan olahraga</li>
                        <li>Pengembangan jaringan Infrastruktur LAN (Intranet dan Internet)</li>
                        <li>Pengembangan sistem informasi sekolah (SIS)</li>
                        <li>Melengkapi sarana dan prasarana perpustakaan dan Lab Komputer</li>
                        <li>Renovasi Aula</li>
                        <li>Renovasi Tampilan Depan Sekolah / Gerbang Sekolah</li>
                        <li>Renovasi Koridor</li>
                    </ol>
                    </p>
                </div>
            </div>
        </div>
        <!-- end : caraosel -->
    </div>
    <!-- start : content article -->
    <div class="container">
        <div class="pt-5">
            <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
            <div class="row" style="">
                <?php foreach ($post as $item): ?>
                    <div class="col-md-4 pb-3 text-left">
                        <div class="containera card">
                            <div class="CoverImage FlexEmbed FlexEmbed--2by1 lazy"
                                 data-src="<?=base_url() . 'uploads/img/cover_post/' . $item->image?>"></div>
                            <div class="card-body" style="padding:20px 30px;">
                                <h6 class="">
                                    <b><?=$item->title;?></b>
                                </h6>
                                <div class="">
                                    <?php
$string = strip_tags($item->content);
if (strlen($string) > 200) {

    // truncate string
    $stringCut = substr($string, 0, 100);
    $endPoint = strrpos($stringCut, ' ');

    //if the string doesn't contain any space then it will cut without word basis.
    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
    $string .= '...';
}
echo $string;
?>
                            </div>
                                <div class="d-flex justify-content-end">
                                    <i><?=$item->name;?></i>
                                </div>
                            </div>
                            <div class="overlay">
                                <div class="card-body">
                                    <div class="mb-4"><i><?=date('d F Y', strtotime($item->created_at));?></i></div>
                                    <a class="text-dark"
                                       href="<?=base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug?>">
                                        <h5><b><?=$item->title;?></b></h5>
                                    </a>
                                    <?php
$string = strip_tags($item->content);
if (strlen($string) > 500) {

    // truncate string
    $stringCut = substr($string, 0, 200);
    $endPoint = strrpos($stringCut, ' ');

    //if the string doesn't contain any space then it will cut without word basis.
    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
    $string .= '...';
}
echo $string;
?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
    <!-- end : content article -->
</div>