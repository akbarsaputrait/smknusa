<div class="<?= $color; ?>">
    <div class="container">
        <div class="text-center text-white mb-4">
            <a hre="" id="link_post" style="text-decoration: none;">
                <h1 data-title="<?= $class[0]->name; ?>">
                    <b><?= $class[0]->name; ?></b>
                </h1>
            </a>
            <h6>
                Sejak tahun <?= $class[0]->since; ?>
            </h6>
        </div>

        <div class="card">
            <div class="slider single-item">
                <?php foreach ($image as $item): ?>
                    <div class="CoverImage FlexEmbed FlexEmbed--0by1 lazy"
                         data-src="<?= base_url() . 'uploads/img/jurusan/' . $item->image ?>"></div>
                <?php endforeach; ?>
            </div>
            <div class="content-blog">
                <div class="row mb-5 align-middle align-items-center">
                    <div class="col-md-6">
                        <span class="text-muted">Share Post</span>
                        <div class="mt-3">
                            <div id="sharePost"></div>
                        </div>
                    </div>
                </div>
                <div class="text-blog">
                    <h3>
                        <b>Tentang</b>
                    </h3>
                    <p><?= $class[0]->content; ?></p>
                </div>
            </div>
        </div>


        <div class="pt-5">
            <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
            <div class="row" style="">
                <?php foreach ($post as $item): ?>
                    <div class="col-md-4 pb-3 text-left">
                        <div class="containera card">
                            <div class="CoverImage FlexEmbed FlexEmbed--2by1 lazy"
                                 data-src="<?= base_url() . 'uploads/img/cover_post/' . $item->image ?>"></div>
                            <div class="card-body" style="padding:20px 30px;">
                                <h6 class="">
                                    <b><?= $item->title; ?></b>
                                </h6>
                                <div class="">
                                    <?php
                                        $string = strip_tags($item->content);
                                        if (strlen($string) > 200) {

                                            // truncate string
                                            $stringCut = substr($string, 0, 100);
                                            $endPoint = strrpos($stringCut, ' ');

                                            //if the string doesn't contain any space then it will cut without word basis.
                                            $string = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
                                            $string .= '...';
                                        }
                                        echo $string;
                                    ?>
                            </div>
                                <div class="d-flex justify-content-end">
                                    <i><?= $item->name; ?></i>
                                </div>
                            </div>
                            <div class="overlay">
                                <div class="card-body">
                                    <div class="mb-4"><i><?= date('d F Y', strtotime($item->created_at)); ?></i></div>
                                    <a class="text-dark"
                                       href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>">
                                        <h5><b><?= $item->title; ?></b></h5>
                                    </a>
                                    <?php
                                    $string = strip_tags($item->content);
                                    if (strlen($string) > 500) {

                                        // truncate string
                                        $stringCut = substr($string, 0, 200);
                                        $endPoint = strrpos($stringCut, ' ');

                                        //if the string doesn't contain any space then it will cut without word basis.
                                        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                        $string .= '...';
                                    }
                                    echo $string;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

</div>
</div>