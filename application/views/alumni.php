<div class="<?= $color; ?>">
    <div class="container">
        <div class="text-center text-white mb-4">
            <h1><b><?= $title; ?></b></h1>
        </div>
        <div class="card">
            <div class="card-body bg-white">
                <?php if ($this->session->flashdata('failed')) {
                    echo $this->session->flashdata('failed');
                } else if ($this->session->flashdata('success')) {
                    echo $this->session->flashdata('success');
                } ?>
                <div class="row">
                    <div class="col-md-12 d-flex justify-content-center">
                        <button class="btn btn-outline-info" data-toggle="modal" data-target="#tambahAlumni">
                            <i class="icon icon-plus"></i> Daftar Alumni
                        </button>
                    </div>
                </div>
                <table id="datatable" class="table table-striped">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Angkatan/Lulus</th>
                        <th>Kuliah</th>
                        <th>Pekerjaan</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $x = 1;
                    foreach ($alumni as $item):
                        ?>
                        <tr>
                            <td><?= $x++; ?></td>
                            <td><?= $item->name; ?></td>
                            <td><?= $item->graduate; ?></td>
                            <td><?= $item->college; ?></td>
                            <td><?= $item->work; ?></td>
                            <td>
                                <div class="btn-group">
                                    <button class="btn btn-outline-info"
                                            data-id="<?= $item->id; ?>"
                                            data-toggle="modal"
                                            data-target="#infoAlumni">
                                        Lihat Profil
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="pt-5">
            <h6 class="text-muted"><i>Artikel Lainnya</i></h6>
            <div class="row" style="">
                <?php foreach ($post as $item): ?>
                    <div class="col-md-4 pb-3 text-left">
                        <div class="containera card">
                            <div class="CoverImage FlexEmbed FlexEmbed--2by1 lazy"
                                 data-src="<?= base_url() . 'uploads/img/cover_post/' . $item->image ?>"></div>
                            <div class="card-body" style="padding:20px 30px;">
                                <h6 class="">
                                    <b><?= $item->title; ?></b>
                                </h6>
                                <?php
                                $string = strip_tags($item->content);
                                if (strlen($string) > 200) {

                                    // truncate string
                                    $stringCut = substr($string, 0, 100);
                                    $endPoint = strrpos($stringCut, ' ');

                                    //if the string doesn't contain any space then it will cut without word basis.
                                    $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string .= '...';
                                }
                                echo $string;
                                ?>
                                <div class="d-flex justify-content-end">
                                    <i><?= $item->name; ?></i>
                                </div>
                            </div>
                            <div class="overlay">
                                <div class="card-body">
                                    <div class="mb-4"><i><?= date('d F Y', strtotime($item->created_at)); ?></i></div>
                                    <a class="text-dark"
                                       href="<?= base_url() . 'post/' . $item->type . '/' . $item->id . '-' . $item->slug ?>">
                                        <h5><b><?= $item->title; ?></b></h5>
                                    </a>
                                    <?php
                                    $string = strip_tags($item->content);
                                    if (strlen($string) > 500) {

                                        // truncate string
                                        $stringCut = substr($string, 0, 200);
                                        $endPoint = strrpos($stringCut, ' ');

                                        //if the string doesn't contain any space then it will cut without word basis.
                                        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                        $string .= '...';
                                    }
                                    echo $string;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tambahAlumni" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Tambah Alumni</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('profile/add_alumni'); ?>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
                </div>

                <div class="form-group">
                    <label>Angkatan/Lulusan</label>
                    <input type="text" name="graduates" class="form-control"
                           placeholder="Tahun">
                </div>

                <div class="form-group">
                    <label>Kuliah</label>
                    <input type="text" name="college" class="form-control"
                           placeholder="Kuliah">
                </div>

                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" name="work" class="form-control"
                           placeholder="Pekerjaan">
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" name="address" class="form-control"
                           placeholder="Alamat">
                </div>

                <div class="form-group">
                    <label>No. Telp</label>
                    <input type="text" name="phone_number" class="form-control"
                           placeholder="08xxxxxxxxx">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control"
                           placeholder="">
                </div>

                <div class="form-group">
                    <label>Pesan</label>
                    <input type="text" name="message" class="form-control"
                           placeholder="Pesan & kesan">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        <i class="icon icon-cursor"></i> Simpan
                    </button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="infoAlumni" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="max-width: 960px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><b>Info Alumni</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('', array('id' => 'edit')); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama Lengkap"
                                   id="info_nama">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Angkatan/Lulusan</label>
                            <input type="text" name="graduate" class="form-control"
                                   placeholder="Tahun" id="info_lulusan">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Telp</label>
                            <input type="text" name="phone_jumber" class="form-control"
                                   placeholder="08xxxxxxxxx" id="info_telp">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kuliah</label>
                            <input type="text" name="college" class="form-control"
                                   placeholder="Kuliah" id="info_kuliah">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pekerjaan</label>
                            <input type="text" name="work" class="form-control"
                                   placeholder="Pekerjaan" id="info_pekerjaan">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="address" class="form-control"
                                   placeholder="Alamat" id="info_alamat">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control"
                                   placeholder="" id="info_email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Pesan</label>
                            <textarea id="info_pesan" style="width: 100%; height: 100px;"></textarea>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
